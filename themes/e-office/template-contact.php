<?php /* Template Name: Contact */
    get_header(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php if (get_field('maps_loc', 'options')):
      $location = get_field('maps_loc', 'options');
      ?>

        <div id="map" class="hero">
          <div id="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
        </div>

    <?php endif; ?>

		<main>

			<div class="container center padding padding-m-0">
				<div class="content contact">

					  <?php include 'includes/module.php'; ?>

				</div>
			</div>

		</main>

	<?php endwhile; else : ?>
		<p><?php _e('Helaas, deze pagina is niet beschikbaar'); ?></p>
	<?php endif; ?>


<?php get_footer(); ?>
