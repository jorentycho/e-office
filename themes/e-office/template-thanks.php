<?php /* Template Name: Bedankt */
    get_header(); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<main>



			<div class="container center padding padding-m-0">
				<div class="content thanks">

          <div class="padding">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="920 -217 280 280">
              <g id="Group_28" data-name="Group 28" transform="translate(920 -217)">
                <path id="Path_27" data-name="Path 27" class="cls-1" d="M2038.3,1662.5Z" transform="translate(-1819.086 -1483.702)"/>
                <path id="Path_28" data-name="Path 28" class="cls-2" d="M2038.3,1662.5Z" transform="translate(-1819.086 -1483.702)"/>
                <rect id="Rectangle_4" data-name="Rectangle 4" class="cls-2" transform="translate(219.214 178.798)"/>
                <path id="Path_29" data-name="Path 29" class="cls-2" d="M2038.3,1662.5Z" transform="translate(-1819.086 -1483.702)"/>
                <rect id="Rectangle_5" data-name="Rectangle 5" class="cls-2" transform="translate(219.214 178.798)"/>
                <path id="Path_30" data-name="Path 30" class="cls-2" d="M2038.3,1662.5Z" transform="translate(-1819.086 -1483.702)"/>
                <rect id="Rectangle_6" data-name="Rectangle 6" class="cls-2" transform="translate(219.214 178.798)"/>
                <g id="Group_27" data-name="Group 27">
                  <g id="Group_26" data-name="Group 26">
                    <g id="Group_24" data-name="Group 24" transform="translate(0.481)">
                      <path id="Path_31" data-name="Path 31" class="cls-3" d="M283.989,139.995A140.007,140.007,0,0,1,143.984,280q-3.872,0-7.7-.215A139.877,139.877,0,0,1,27.854,218.214h0V150.438c0-3.958-23.7-18.315-23.381-22.187a140.009,140.009,0,0,1,279.118,1.119C283.849,132.875,283.989,136.424,283.989,139.995Z" transform="translate(-4.47)"/>
                    </g>
                    <g id="Group_25" data-name="Group 25" transform="translate(0 37.154)">
                      <path id="Path_32" data-name="Path 32" class="cls-4" d="M174.9,473.268c1.269,2.753,5.679,8.109,3.829,13.745-2.14,6.517-9.034,7.378-9.034,10.561,0,1.581.968,7.851-.1,10.378-2.312,5.474-10.583,10.647-20.262,11.583-13.788,1.334-36.265,3.786-87.318-5.464h0c-.387.032-1.85.1-6.539.183-2.925.054,1.592,12.024,1.592,12.024-8.668.161-20.886.237-33.189.247h0A139.21,139.21,0,0,1,0,448.306q0-5.937.484-11.744c18.111.054,38.029.032,50.451-.247,0,0-3.377,5.463.581,5.367,11.389-.28,10.841-.527,11.83-.688,3.9-.613,9.26-2.882,14.358-14.54,9.582-21.9,10.5-28.285,18.713-33.759,4.28-2.861,8.313-6.206,11.271-13.3,2.334-5.625,3.99-13.6,4.528-25.543.086-1.861,1.914-7.614,4.108-8.088,5.4-1.151,14.863.57,19.186,14.39.925,2.958,2.474,13.712-.516,22a133.246,133.246,0,0,1-9.819,21.036c-6.56,9.884,1.291,12.314,2.355,12.314h40.535a13.613,13.613,0,0,1,8.464,2.237c3.818,2.667,7.023,8.529,6.926,12.916-.2,8.9-7.636,12.024-7.636,14.96,0,1.484,7.862,6.915,7.636,13.551C183.218,465.622,173.56,470.354,174.9,473.268Z" transform="translate(0 -345.466)"/>
                    </g>
                  </g>
                </g>
              </g>
            </svg>

            <h1><?php the_title(); ?></h1>

            <?php the_content(); ?>
          </div>

				</div>
			</div>


		</main>

	<?php endwhile; else : ?>
		<p><?php _e('Helaas, deze pagina is niet beschikbaar'); ?></p>
	<?php endif; ?>


<?php get_footer(); ?>
