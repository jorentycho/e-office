<?php /* Template Name: Producten */
  get_header(); ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
		$root = 'includes/template-parts/'; ?>

		<main>
      <?php include 'includes/heading.php'; ?>

			<div class="container center padding padding-m-0">
				<div class="content products">

          <div class="padding">
            <?php the_content(); ?>
          </div>

          <?php $terms = get_terms('productgroep', array(
              'hide_empty' => true,
              'orderby' => 'term_id',
          )); ?>

          <?php foreach ($terms as $term) { ?>
            <?php $args = array(
              'post_type' => 'producten',
              'posts_per_page' => -1,
              'order' => 'ASC',
              'tax_query' => array(
                array(
                  'taxonomy' => 'productgroep',
                  'field'    => 'id',
                  'terms'    => $term->term_id,
                ),
              ),
            ); ?>

            <?php include $root.'product_cat.php'; ?>

          <?php } ?>


          <?php include 'includes/module.php'; ?>

				</div>
			</div>

		</main>

	<?php endwhile; else : ?>
		<p><?php _e('Helaas, deze pagina is niet beschikbaar'); ?></p>
	<?php endif; ?>


<?php get_footer(); ?>
