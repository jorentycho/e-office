<?php get_header(); ?>

  <?php
    $root = 'includes/template-parts/';
    $curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
    $user_id = 'user_'. $curauth->ID;
  ?>
		<main>

      <div class="container center padding padding-m-0">
        <div class="padding">
          <h1 <?php if(get_field('light')): ?>class="light"<?php endif; ?>><?php echo get_the_author_meta( 'first_name', $curauth->ID); ?> <?php echo get_the_author_meta( 'last_name', $curauth->ID); ?></h1>
        </div>
      </div>



			<div class="container center padding padding-m-0">
				<div class="content">

          <div class="col-4 col-t-12 right padding">
            <article class="module__user">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>/wie-we-zijn">
                <div class="arrow-back">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="-143 2710 18.985 11.009" class="filter--button-arrow">
                    <g transform="translate(-142.996 2710.009)">
                      <path d="M18.7.1a.494.494,0,0,0-.7.1L9.5,9.8,1,.2A.494.494,0,0,0,.3.1C0,.2-.1.5.1.7L9,10.7a.55.55,0,0,0,.5.3h0a.52.52,0,0,0,.4-.2L18.8.8C19.1.5,19,.2,18.7.1Z"></path>
                    </g>
                  </svg>
                </div>
              </a>
              <?php $image = get_field('image', $user_id);
              $size = 'large';
              $portrait = $image['sizes'][$size]; ?>

              <div class="module__user__portrait module__user__portrait--single" style="background-image: url('<?php echo $portrait; ?>')"></div>
              <h4><?php echo get_the_author_meta( 'first_name', $curauth->ID); ?> <?php echo get_the_author_meta( 'last_name', $curauth->ID); ?></h4>
              <div class="job"><?php the_field('function', $user_id); ?></div>
              <?php the_field('description', $user_id); ?>
              <div class="social-icons">

                <?php if( get_field('linkedin', $user_id) ): ?>
                <a href="<?php the_field('linkedin', $user_id); ?>" target="_blank" class="social-icons--linkedin" title="LinkedIn"><svg viewBox="0 0 512 512"><path d="M186.4 142.4c0 19-15.3 34.5-34.2 34.5 -18.9 0-34.2-15.4-34.2-34.5 0-19 15.3-34.5 34.2-34.5C171.1 107.9 186.4 123.4 186.4 142.4zM181.4 201.3h-57.8V388.1h57.8V201.3zM273.8 201.3h-55.4V388.1h55.4c0 0 0-69.3 0-98 0-26.3 12.1-41.9 35.2-41.9 21.3 0 31.5 15 31.5 41.9 0 26.9 0 98 0 98h57.5c0 0 0-68.2 0-118.3 0-50-28.3-74.2-68-74.2 -39.6 0-56.3 30.9-56.3 30.9v-25.2H273.8z"/></svg><!--[if lt IE 9]><em>LinkedIn</em><![endif]--></a>
                <?php endif; ?>

                <?php if( get_field('twitter', $user_id) ): ?>
                <a href="<?php the_field('twitter', $user_id); ?>" target="_blank" class="social-icons--twitter" title="Twitter"><svg viewBox="0 0 512 512"><path d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z"/></svg><!--[if lt IE 9]><em>Twitter</em><![endif]--></a>
                <?php endif; ?>

                <?php if( get_field('facebook', $user_id) ): ?>
                <a href="<?php the_field('facebook', $user_id); ?>" target="_blank" class="social-icons--facebook" title="Facebook"><svg viewBox="0 0 512 512"><path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"/></svg><!--[if lt IE 9]><em>Facebook</em><![endif]--></a>
                <?php endif; ?>

                <?php if( get_field('mobile', $user_id) ): ?>
                <a href="tel:<?php the_field('mobile', $user_id); ?>" target="_blank" class="social-icons--mobile" title="Mobile phone"><svg viewBox="3754 2510 32 32.014"><g transform="translate(3753.9 2510.014)"><g><path d="M27.3.5a2.249,2.249,0,0,0-3.2.3c-.3.5-3.9,5.8-4.3,6.5a1.689,1.689,0,0,0-.3,1,3.429,3.429,0,0,0,.4,1.6c.3.5,1.2,2.1,1.6,2.8a40.838,40.838,0,0,1-4.2,4.7,52.546,52.546,0,0,1-4.7,4.2c-.8-.4-2.3-1.3-2.8-1.6a2.731,2.731,0,0,0-2.6-.2c-.6.4-5.9,3.9-6.4,4.3a1.865,1.865,0,0,0-.7,1.6,3.32,3.32,0,0,0,.5,1.6S4,32,5.9,32c5.5-.2,11.9-5.5,16.3-9.9s9.7-10.8,9.9-16.2h0C32,3.9,27.3.6,27.3.5ZM20.7,20.7C14.9,26.5,9.6,29.8,5.9,30a16.469,16.469,0,0,1-3.8-3.8c-.1-.2-.2-.3-.2-.5v-.1c.8-.6,5.4-3.6,6.2-4.1a.988.988,0,0,1,.6.2c.3.2,1.4.8,2.8,1.6l1.1.6,1-.7a29.779,29.779,0,0,0,5-4.4,34.63,34.63,0,0,0,4.4-5l.7-1-.6-1.1c-.6-1.1-1.3-2.4-1.6-2.8a1.191,1.191,0,0,1-.2-.6h0c.7-.8,3.8-5.5,4.4-6.3a1.087,1.087,0,0,1,.5.1,15.465,15.465,0,0,1,3.9,3.8C29.8,9.7,26.5,14.9,20.7,20.7Z"/></g></g></svg><!--[if lt IE 9]><em>Phone</em><![endif]--></a>
                <?php endif; ?>

                <a href="mailto:<?php echo $curauth->data->user_email; ?>" class="social-icons--email" title="E-mail"><svg viewBox="0 0 512 512"><path d="M101.3 141.6v228.9h0.3 308.4 0.8V141.6H101.3zM375.7 167.8l-119.7 91.5 -119.6-91.5H375.7zM127.6 194.1l64.1 49.1 -64.1 64.1V194.1zM127.8 344.2l84.9-84.9 43.2 33.1 43-32.9 84.7 84.7L127.8 344.2 127.8 344.2zM384.4 307.8l-64.4-64.4 64.4-49.3V307.8z"/></svg><!--[if lt IE 9]><em>Email</em><![endif]--></a>

              </div>
              <?php $the_query = new WP_Query( array('post_type' => 'post', 'posts_per_page' => 3, 'ignore_sticky_posts' => true, 'author' => $curauth->ID) ); ?>

              <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                <?php include $root .'post_list_basic.php'; ?>

              <?php endwhile; wp_reset_postdata(); endif; ?>

            </article>
          </div>
          <div class="col-8 col-t-12 left people-module">
            <?php include $root .'user_list.php'; ?>
          </div>


          <div class="clearfix"></div>

				</div>
			</div>

		</main>


<?php get_footer(); ?>
