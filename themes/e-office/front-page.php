<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<main>
      <script type="text/javascript">
        var post_id = <?php echo $post->ID; ?>
      </script>
			<div class="container center padding">
				<div class="col-6 col-d-12 left padding front-page">
					<?php include 'includes/chatbot.php'; ?>
				</div>

				<div class="col-6 col-d-0 left module__fp--sticky-posts">
					<?php $stickies = get_option( 'sticky_posts' );
					if ( $stickies ) {
					    $args = [
					        'post_type'           => 'post',
					        'post__in'            => $stickies,
					        'posts_per_page'      => 3,
					        'ignore_sticky_posts' => 1
					    ];
					    $the_query = new WP_Query($args);

					    if ( $the_query->have_posts() ) {
					        while ( $the_query->have_posts() ) {
					            $the_query->the_post();

					            ?>
							<div class="col-t-12 left module__fp--sticky-posts--single">
								<?php $cat = get_the_terms($post->ID, 'type'); ?>

								<p class="module__fp--sticky-posts--single--cat"><?php echo $cat[0]->name; ?></p>
					            <h3 class="module__fp--sticky-posts--single--title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
											<?php
						            $content = get_the_content();

						            $rows = get_field('blocks');

						            if( $rows ) {

													if(!empty($rows[0]['text_column'])) {
														$content = $rows[0]['text_column'];
													}

						            }


												$content = strip_tags($content, '<br />');
												echo '<p>'. shorten_string($content, 40) .'</p>';
						          ?>
					            <a href="<?php echo get_permalink(); ?>"> lees verder</a>
					        </div>


					            <?php

					        }
					        wp_reset_postdata();
					    }
					}
					?>
				</div>
			</div>

			<div class="container center padding padding-m-0">
				<div class="content front-page">

						<h1 class="front-page"><?php the_title(); ?></h1>

						<?php include 'includes/module.php'; ?>

				</div>
			</div>

		</main>

	<?php endwhile; else : ?>
		<p><?php _e( 'Helaas, deze pagina is niet beschikbaar' ); ?></p>
	<?php endif; ?>


<?php get_footer(); ?>
