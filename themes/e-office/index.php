<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<main>


			<?php include 'includes/heading.php'; ?>

			<div class="container center padding padding-m-0">
				<div class="content">

						<?php include 'includes/module.php'; ?>

				</div>
			</div>

		</main>

	<?php endwhile; else : ?>
		<p><?php _e( 'Helaas, deze pagina is niet beschikbaar' ); ?></p>
	<?php endif; ?>


<?php get_footer(); ?>
