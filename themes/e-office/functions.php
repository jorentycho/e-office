<?php

/*
  ****************** ACF ******************
*/

    add_action('init', 'acf_eof_init');
    function acf_eof_init()
    {
        // Import ACF
      //define( 'ACF_LITE' , true );

      // Rename options page and register subpages
      if (function_exists('acf_add_options_page')) {
          $page = array(

            'page_title' => 'Extra Instellingen',
            'menu_title' => '',
            'menu_slug' => '',
            'position' => '99.1',
            'parent_slug' => '',
            'icon_url' => false,
            'redirect' => true,
            'post_id' => 'options',
            'autoload' => false,

          );
          acf_add_options_page($page);
      }

        if (function_exists('acf_set_options_page_capability')) {
            acf_set_options_page_capability('read');
        }
    }

    // Google maps key for wp-admin
    function my_acf_init() {

    	acf_update_setting('google_api_key', 'AIzaSyBdNK-Em19WZsmj9I1KfGAr_klEon5Agf8');
    }

    add_action('acf/init', 'my_acf_init');

    // Custom Excerpt function for Advanced Custom Fields
    function custom_field_excerpt() {
        global $post;
        $text = get_field('your_field_name'); //Replace 'your_field_name'
        if ( '' != $text ) {
            $text = strip_shortcodes( $text );
            $text = apply_filters('the_content', $text);
            $text = str_replace(']]&gt;', ']]&gt;', $text);
            $excerpt_length = 20; // 20 words
            $excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
            $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
        }
        return apply_filters('the_excerpt', $text);
    }


    // Extend RSS with custom fields

    function fields_in_feed($content) {
        if(is_feed()) {
            $post_id = get_the_ID();
            $rows = get_field('blocks');
            $cf_content = $rows[0]['text_column'];
            $content = $content.$cf_content;
        }
        return $content;
    }
    add_filter('the_content','fields_in_feed');

/*
  ****************** Register post types ******************
*/


    // Register Producten Post Type
    function eoffice_producten_cpt()
    {
        $args_post = array(
            'label'                 => __('Berichten', 'text_domain'),
            'description'           => __('Berichten', 'text_domain'),
            'public'                => true,
            '_builtin'              => false,
            '_edit_link'            => 'post.php?post=%d',
            'taxonomies'            => array('type', 'theme'),
            'capability_type'       => 'post',
            'map_meta_cap'          => true,
            'hierarchical'          => false,
            'menu_position'         => 4,
            'rewrite'               => array( 'slug' => 'magazine' ),
            'query_var'             => false,
            'supports'               => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats' ),
            // 'show_ui'               => true,
            // 'show_in_menu'          => true,
            // 'show_in_admin_bar'     => true,
            // 'show_in_nav_menus'     => false,
            // 'can_export'            => true,
            // 'has_archive'           => false,
            // 'exclude_from_search'   => false,
            // 'publicly_queryable'    => true,
        );
        register_post_type('post', $args_post);


        $labels = array(
            'name'                  => _x('Producten', 'Post Type General Name', 'text_domain'),
            'singular_name'         => _x('Producten', 'Post Type Singular Name', 'text_domain'),
            'menu_name'             => __('Producten', 'text_domain'),
            'name_admin_bar'        => __('Producten', 'text_domain'),
            'archives'              => __('Producten archief', 'text_domain'),
            'parent_item_colon'     => __('Parent producten:', 'text_domain'),
            'all_items'             => __('Alle producten', 'text_domain'),
            'add_new_item'          => __('Voeg product toe', 'text_domain'),
            'add_new'               => __('Voeg nieuwe toe', 'text_domain'),
            'new_item'              => __('Nieuw product', 'text_domain'),
            'edit_item'             => __('Bewerk product', 'text_domain'),
            'update_item'           => __('Werk product bij', 'text_domain'),
            'view_item'             => __('Bekijk product', 'text_domain'),
            'search_items'          => __('Zoek producten', 'text_domain'),
            'not_found'             => __('Niets gevonden', 'text_domain'),
            'not_found_in_trash'    => __('Niets gevonden', 'text_domain'),
            'featured_image'        => __('Featured Image', 'text_domain'),
            'set_featured_image'    => __('Set featured image', 'text_domain'),
            'remove_featured_image' => __('Remove featured image', 'text_domain'),
            'use_featured_image'    => __('Use as featured image', 'text_domain'),
            'insert_into_item'      => __('Insert into item', 'text_domain'),
            'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
            'items_list'            => __('Items list', 'text_domain'),
            'items_list_navigation' => __('Items list navigation', 'text_domain'),
            'filter_items_list'     => __('Filter items list', 'text_domain'),
        );
        $args = array(
            'label'                 => __('Producten', 'text_domain'),
            'description'           => __('Producten', 'text_domain'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail', 'revisions' ),
            'taxonomies'            => array('productgroep'),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
            'menu_icon'             => 'dashicons-products',
        );
        register_post_type('producten', $args);

        $labels_jobs = array(
            'name'                  => _x('Vacatures', 'Post Type General Name', 'text_domain'),
            'singular_name'         => _x('Vacature', 'Post Type Singular Name', 'text_domain'),
            'menu_name'             => __('Vacatures', 'text_domain'),
            'name_admin_bar'        => __('Vacatures', 'text_domain'),
            'archives'              => __('Vacature archief', 'text_domain'),
            'parent_item_colon'     => __('Parent vacature:', 'text_domain'),
            'all_items'             => __('Alle vacatures', 'text_domain'),
            'add_new_item'          => __('Voeg vacature toe', 'text_domain'),
            'add_new'               => __('Voeg nieuwe toe', 'text_domain'),
            'new_item'              => __('Nieuwe vacature', 'text_domain'),
            'edit_item'             => __('Bewerk vacature', 'text_domain'),
            'update_item'           => __('Werk vacature bij', 'text_domain'),
            'view_item'             => __('Bekijk vacature', 'text_domain'),
            'search_items'          => __('Zoek vacatures', 'text_domain'),
            'not_found'             => __('Niets gevonden', 'text_domain'),
            'not_found_in_trash'    => __('Niets gevonden', 'text_domain'),
            'featured_image'        => __('Featured Image', 'text_domain'),
            'set_featured_image'    => __('Set featured image', 'text_domain'),
            'remove_featured_image' => __('Remove featured image', 'text_domain'),
            'use_featured_image'    => __('Use as featured image', 'text_domain'),
            'insert_into_item'      => __('Insert into item', 'text_domain'),
            'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
            'items_list'            => __('Items list', 'text_domain'),
            'items_list_navigation' => __('Items list navigation', 'text_domain'),
            'filter_items_list'     => __('Filter items list', 'text_domain'),
        );
        $args_jobs = array(
            'label'                 => __('Vacatures', 'text_domain'),
            'description'           => __('Vacatures', 'text_domain'),
            'labels'                => $labels_jobs,
            'supports'              => array('title', 'editor', 'thumbnail', 'revisions' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
            'menu_icon'             => 'dashicons-groups',
        );
        register_post_type('vacatures', $args_jobs);

        $labels_partners = array(
            'name'                  => _x('Partners', 'Post Type General Name', 'text_domain'),
            'singular_name'         => _x('Partner', 'Post Type Singular Name', 'text_domain'),
            'menu_name'             => __('Partners', 'text_domain'),
            'name_admin_bar'        => __('Partners', 'text_domain'),
            'archives'              => __('Partner archief', 'text_domain'),
            'parent_item_colon'     => __('Parent partner:', 'text_domain'),
            'all_items'             => __('Alle partners', 'text_domain'),
            'add_new_item'          => __('Voeg partner toe', 'text_domain'),
            'add_new'               => __('Voeg nieuwe toe', 'text_domain'),
            'new_item'              => __('Nieuwe partner', 'text_domain'),
            'edit_item'             => __('Bewerk partner', 'text_domain'),
            'update_item'           => __('Werk partner bij', 'text_domain'),
            'view_item'             => __('Bekijk partner', 'text_domain'),
            'search_items'          => __('Zoek partners', 'text_domain'),
            'not_found'             => __('Niets gevonden', 'text_domain'),
            'not_found_in_trash'    => __('Niets gevonden', 'text_domain'),
            'featured_image'        => __('Featured Image', 'text_domain'),
            'set_featured_image'    => __('Set featured image', 'text_domain'),
            'remove_featured_image' => __('Remove featured image', 'text_domain'),
            'use_featured_image'    => __('Use as featured image', 'text_domain'),
            'insert_into_item'      => __('Insert into item', 'text_domain'),
            'uploaded_to_this_item' => __('Uploaded to this item', 'text_domain'),
            'items_list'            => __('Items list', 'text_domain'),
            'items_list_navigation' => __('Items list navigation', 'text_domain'),
            'filter_items_list'     => __('Filter items list', 'text_domain'),
        );
        $args_partners = array(
            'label'                 => __('Partners', 'text_domain'),
            'description'           => __('Partners', 'text_domain'),
            'labels'                => $labels_partners,
            'supports'              => array('title', 'editor', 'thumbnail', 'revisions' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
            'menu_icon'             => 'dashicons-admin-users',
        );
        register_post_type('partners', $args_partners);

    }
    add_action('init', 'eoffice_producten_cpt', 0);


/*
  ****************** taxonomies ******************
*/
    // Product groups
    function create_productgroups()
    {
        register_taxonomy(
            'productgroep',
            'producten',
            array(
                'label' => 'Productgroep',
                'labels' => array(
                    'add_new_item' => 'Nieuwe productgroep',
                    'search_items' => 'Productgroepen zoeken',
                    'menu_name' => 'Productgroepen',
                    'all_items' => 'Alle productgroepen',

                ),
                'rewrite' => array( 'slug' => 'productgroepen' ),
                'hierarchical' => true,
                'show_in_menu' => true,
                'show_ui' => true,
                'show_in_nav_menus' => true,
                'show_admin_column' => true,
                'show_in_quick_edit' => true,

            )
        );

    }
    add_action('init', 'create_productgroups');

    // Magazine type
    function create_magazine_types()
    {
        register_taxonomy(
            'type',
            'post',
            array(
                'label' => 'Type',
                'labels' => array(
                    'add_new_item' => 'Nieuw type',
                    'search_items' => 'Types zoeken',
                    'menu_name' => 'Types',
                    'all_items' => 'Alle types',

                ),
                'rewrite' => array( 'slug' => 'types' ),
                'hierarchical' => true,
                'show_in_menu' => true,
                'show_ui' => true,
                'show_in_nav_menus' => true,
                'show_admin_column' => true,
                'show_in_quick_edit' => true,

            )
        );

    }
    add_action('init', 'create_magazine_types');

    // Magazine theme
    function create_magazine_theme()
    {
        register_taxonomy(
            'theme',
            'post',
            array(
                'label' => 'Thema',
                'labels' => array(
                    'add_new_item' => 'Nieuw thema',
                    'search_items' => 'thema\'s zoeken',
                    'menu_name' => 'Thema\'s',
                    'all_items' => 'Alle thema\'s',

                ),
                'rewrite' => array( 'slug' => 'thema' ),
                'hierarchical' => true,
                'show_in_menu' => true,
                'show_ui' => true,
                'show_in_nav_menus' => true,
                'show_admin_column' => true,
                'show_in_quick_edit' => true,

            )
        );

    }
    add_action('init', 'create_magazine_theme');

    //deregister category
    function ev_unregister_taxonomy(){
        register_taxonomy('category', array());
    }
    add_action('init', 'ev_unregister_taxonomy');


/*
  ****************** REDIRECTING MAGAZINE ******************
*/

    // remove_filter('template_redirect', 'redirect_canonical');
    // add_action('init', 'acf_init');
    //
    //
    // add_filter( 'post_link', 'magazinePermalinks', 10, 3);
    //
    // function magazinePermalinks($permalink, $post) {
    //   if($post->post_type == 'post') {
    //     return str_replace(get_site_url() , get_site_url().'/magazine', $permalink);
    //   }
    //
    //   return $permalink;
    // }




    function admin_style() { ?>
      <style>
      #adminmenu li.menu-icon-post:not(.menu-top-first) {
        display: none;
      }
      </style>
    <?php }
    add_action( 'admin_print_styles', 'admin_style' );

/*
  ****************** TRANSFER POST STUFF ******************
*/
  // add_action('init', 'move_thumbnail_id');
  // function move_thumbnail_id() {
  //   $args = array( 'post_type' => 'post', 'posts_per_page' => -1, 'post_status' => 'any');
  //   $posts = get_posts($args);
  //
  //   foreach ( $posts as $post ) {
  //     $img = get_post_meta($post->ID, 'image');
  //
  //     // echo '<pre>';
  //     // print_r($img['0']);
  //     // echo '</pre>';
  //
  //     $set_it = add_post_meta($post->ID, '_thumbnail_id', $img['0'], true);
  //
  //     if ( is_wp_error( $set_it ) ) {
  //     	echo 'There was an error somewhere and the terms couldn\'t be set.';
  //     } else {
  //     	echo 'Success! The post\'s categories were set.';
  //     }
  //
  //   }
  // }


  // add_action('init', 'move_categories');
  // function move_categories() {
  //
  //   $args = array( 'post_type' => 'post', 'posts_per_page' => -1, 'post_status' => 'any');
  //   $posts = get_posts($args);
  //   $i = 1;
  //
  //
  //   foreach ( $posts as $post ) {
  //     //print_r($post->ID .' ');
  //     $type =  get_field('type', $post->ID);
  //     //echo $type .' ';
  //     if($type == 'reference') {
  //
  //       echo $i;
  //
  //       $cat_ids = array( 30 );
  //
  //       $term_taxonomy_ids = wp_set_object_terms( $post->ID, $cat_ids, 'type', false );
  //
  //       echo 'changing the terms';
  //
  //       $i++;
  //     }
  //   }
  // }



  // add_action('init', 'move_tags');
  // function move_tags() {
  //
  //   $args = array( 'post_type' => 'tag', 'posts_per_page' => -1, 'post_status' => 'any');
  //   $tags = get_posts($args);
  //   $i = 0;
  //
  //
  //   foreach ( $tags as $tag ) {
  //
  //       $set_term = wp_insert_term(
  //         $tag->post_title, // the term
  //         'post_tag' // the taxonomy
  //       );
  //
  //       if ( is_wp_error( $set_term ) ) {
  //       	echo 'There was an error somewhere and the terms couldn\'t be set.';
  //       } else {
  //       	echo 'Success! The post\'s categories were set.';
  //       }
  //
  //       $i++;
  //   }
  // }


  // add_action('init', 'set_tags');
  // function set_tags() {
  //
  //   $args = array( 'post_type' => 'post', 'posts_per_page' => -1, 'post_status' => 'any');
  //   $posts = get_posts($args);
  //
  //
  //   foreach ( $posts as $post ) {
  //
  //     $custom_tags = Custom_Tag_Plugin::get_tags($post->ID, 'post');
  //     $magazine_tags = Custom_Tag_Plugin::get_tags($post->ID, 'magazine');
  //
  //     $tags = array_merge($custom_tags, $magazine_tags);
  //
  //     $tag_list = array();
  //
  //     foreach ($tags as $tag) {
  //       array_push($tag_list, $tag->name);
  //     }
  //
  //     //print_r($tag_list);
  //
  //     $term_taxonomy_ids = wp_set_object_terms( $post->ID, $tag_list, 'post_tag', false );
  //
  //   }
  // }

/*
  ****************** Navigation ******************
*/

    // Register nav menu's
    add_action('after_setup_theme', 'navigation_registration');
    function navigation_registration()
    {
        register_nav_menu('primary', __('Top menu', 'primary'));
        register_nav_menu('footer', __('Footer', 'footer'));
        register_nav_menu('mini footer', __('Mini footer', 'mini footer'));
    }


    function active_menus_for_single( $classes, $item, $args ) {
    	if( 'primary' !== $args->theme_location )
    		return $classes;
    	if( ( is_singular( 'post' ) && !has_term('klantverhaal', 'type') ) && 'Magazine' == $item->title )
    		$classes[] = 'current-menu-item';

      if( ( is_singular( 'post' ) && has_term('klantverhaal', 'type') ) && 'klanten' == $item->title )
    		$classes[] = 'current-menu-item';

    	if( is_singular( 'producten' ) && 'Producten' == $item->title )
    		$classes[] = 'current-menu-item';

      if( is_singular( 'vacatures' ) && 'werken bij' == $item->title )
    		$classes[] = 'current-menu-item';

    	if( is_author() && 'wie we zijn' == $item->title )
    		$classes[] = 'current-menu-item';

    	return array_unique( $classes );
    }
    add_filter( 'nav_menu_css_class', 'active_menus_for_single', 10, 3 );


/*
  ****************** Remove support ******************
*/
    if (! function_exists('solarmade_cleaner_wordpress')) {
        function solarmade_cleaner_wordpress()
        {
            remove_action('wp_head', 'rsd_link');
            remove_action('wp_head', 'wp_generator');
            remove_action('wp_head', 'feed_links', 2);
            remove_action('wp_head', 'feed_links_extra', 3);
            remove_action('wp_head', 'index_rel_link');
            remove_action('wp_head', 'wlwmanifest_link');
            remove_action('wp_head', 'start_post_rel_link', 10, 0);
            remove_action('wp_head', 'parent_post_rel_link', 10, 0);
            remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
            remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
            remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
            remove_action('admin_print_styles', 'print_emoji_styles');
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('admin_print_scripts', 'print_emoji_detection_script');
            remove_action('wp_print_styles', 'print_emoji_styles');
            remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
            remove_filter('the_content_feed', 'wp_staticize_emoji');
            remove_filter('comment_text_rss', 'wp_staticize_emoji');
            remove_action('rest_api_init', 'wp_oembed_register_route');
            remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
            remove_action('wp_head', 'wp_oembed_add_discovery_links');
            remove_action('wp_head', 'wp_oembed_add_host_js');

            // Added by hand
            add_filter('emoji_svg_url', '__return_false');
            remove_action('wp_head', 'rest_output_link_wp_head', 10);
            remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);

            if ( !is_admin() ):
              wp_deregister_script('jquery');
              wp_deregister_script('wp-mediaelement');
              wp_deregister_style('wp-mediaelement');
            endif;
        }
        add_action('init', 'solarmade_cleaner_wordpress');
    }

    if (! function_exists('user_profile_cleanup')) {
        function user_profile_cleanup() {
          remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
        }
        add_action('init', 'user_profile_cleanup');
    }


    add_action( 'admin_print_styles-profile.php', 'remove_profile_fields' );
    add_action( 'admin_print_styles-user-edit.php', 'remove_profile_fields' );

    function remove_profile_fields( $hook ) {
        ?>
        <style type="text/css">
            h2:nth-of-type(1),
            h2:nth-of-type(4) {
              display: none;
            }
            tr.user-rich-editing-wrap,
            tr.user-comment-shortcuts-wrap,
            tr.user-admin-bar-front-wrap,
            tr.user-language-wrap,
            tr.user-url-wrap,
            tr.user-profile-picture,
            tr.user-description-wrap
            {
              display: none;
            }
        </style>
        <?php
    }

/*
  ****************** Extra's ******************
*/


    add_action('wp_ajax_views_hit', 'views_hit');
    add_action('wp_ajax_nopriv_views_hit', 'views_hit');

    function views_hit() {
        global $wpdb;
        $post_id = intval($_GET['post']);

        if($post_id > 0) {
          $row = $wpdb->get_row('SELECT views FROM wp_blog_views WHERE post_id = '.$post_id, ARRAY_A);
          if($row) $views = $row['views'];
          else $views = 0;

          $views++;

          if(!$row) {
            $wpdb->insert(
              $wpdb->prefix.'blog_views',
              array(
                  'views'    => $views,
                  'post_id' => $post_id
              )
            );
          }
          else
          {
            $wpdb->update(
              $wpdb->prefix.'blog_views',
              array(
                  'views'    => $views
              ),
              array('post_id' => $post_id)
            );
          }
        }

        echo $views;
        die();
    }

    function get_post_views( $post_id ) {
      global $wpdb;
     	$row = $wpdb->get_row('SELECT views FROM wp_blog_views WHERE post_id = '.$post_id, ARRAY_A);

     	return intval($row['views']);
    }

    add_filter('manage_post_posts_columns', 'posts_column_views');
    add_action('manage_post_posts_custom_column', 'posts_custom_column_views',5,2);
    function posts_column_views($defaults){
        $defaults['post_views'] = __('Views');
        return $defaults;
    }
    function posts_custom_column_views($column_name, $id){
        if($column_name === 'post_views'){
            echo get_post_views(get_the_ID());
        }
    }


    // Add theme support
    function custom_stuff()
    {
        // post thumbnail
      add_theme_support('post-thumbnails');
        add_image_size('hero', 2000, 9999);
        add_image_size('vision', 500, 500);

        add_image_size('user_s', 225, 225, true);

        add_image_size('partners', 150, 45);
        add_image_size('icon', 30, 30);

        add_image_size('magazine_thumb', 300, 9999);

      // html5 searchform
      add_theme_support('html5', array( 'search-form' ));
    }
    add_action('after_setup_theme', 'custom_stuff');

    function extend_search($posts) {
      if(is_search() && !is_admin()) {
        $search = sanitize_text_field( get_query_var( 's' ) );
        $args = array(
          'count_total' => false,
          'search' => sprintf( '*%s*', $search ),
          'search_fields' => array(
            'display_name',
            'user_login',
          ),
          'fields' => 'all'
        );


        $matching_users = get_users( $args );

        if(count($matching_users)) {
            foreach($matching_users as $u) {
            $post  = new \stdClass;

            $post->ID = $u->ID;
            $post->post_title = $u->display_name;
            $post->post_type = 'auteur';
            $post->post_name = 'auteur/'.$u->user_login;
            $post->post_content = get_user_meta($u->ID,'description', true);
            array_push($posts, $post);
          }
        }
      }

      return $posts;
    }

    add_filter('the_posts', 'extend_search');


    // Add SVG support for media uploader
    add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {

      global $wp_version;
      if ( $wp_version !== '4.7.1' ) {
         return $data;
      }

      $filetype = wp_check_filetype( $filename, $mimes );

      return [
          'ext'             => $filetype['ext'],
          'type'            => $filetype['type'],
          'proper_filename' => $data['proper_filename']
      ];

    }, 10, 4 );

    function cc_mime_types( $mimes ){
      $mimes['svg'] = 'image/svg+xml';
      return $mimes;
    }
    add_filter( 'upload_mimes', 'cc_mime_types' );

    function fix_svg() {
      echo '<style type="text/css">
            .attachment-266x266, .thumbnail img {
                 width: 100% !important;
                 height: auto !important;
            }
            </style>';
    }
    add_action( 'admin_head', 'fix_svg' );


    // Change Excerpt length
    function custom_excerpt_length($length)
    {
        return 55;
    }
    add_filter('excerpt_length', 'custom_excerpt_length', 999);


    // Change Excerpt ending
    function new_excerpt_more($more)
    {
        return '';
    }
    add_filter('excerpt_more', 'new_excerpt_more');

    // Hide content editor from vision page
    add_action( 'admin_init', 'hide_editor' );
    function hide_editor() {
            $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
            if( !isset( $post_id ) ) return;
            $template_file = get_post_meta($post_id, '_wp_page_template', true);
        if($template_file == 'template-vision.php'){
            remove_post_type_support('page', 'editor');
        }
    }

    // Change author url base
    add_action('init', 'author_url_base');
    function author_url_base()
    {
        global $wp_rewrite;
        $wp_rewrite->author_base = 'auteur';
        $wp_rewrite->author_structure = '/' . $wp_rewrite->author_base . '/%author%';
    }

    /*
      ****************** Include ******************
    */

    include 'functions/user-taxonomy.php';

  // =========================================================================
  // Custom duplicate post type
  // https://rudrastyh.com/wordpress/duplicate-post.html
  // =========================================================================
  /*
   * Function creates post duplicate as a draft and redirects then to the edit post screen
   */
  function rd_duplicate_post_as_draft(){
    global $wpdb;
    if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
      wp_die('No post to duplicate has been supplied!');
    }

    /*
     * get the original post id
     */
    $post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
    /*
     * and all the original post data then
     */
    $post = get_post( $post_id );

    /*
     * if you don't want current user to be the new post author,
     * then change next couple of lines to this: $new_post_author = $post->post_author;
     */
    $current_user = wp_get_current_user();
    $new_post_author = $current_user->ID;

    /*
     * if post data exists, create the post duplicate
     */
    if (isset( $post ) && $post != null) {

      /*
       * new post data array
       */
      $args = array(
        'comment_status' => $post->comment_status,
        'ping_status'    => $post->ping_status,
        'post_author'    => $new_post_author,
        'post_content'   => $post->post_content,
        'post_excerpt'   => $post->post_excerpt,
        'post_name'      => $post->post_name,
        'post_parent'    => $post->post_parent,
        'post_password'  => $post->post_password,
        'post_status'    => 'draft',
        'post_title'     => $post->post_title,
        'post_type'      => $post->post_type,
        'to_ping'        => $post->to_ping,
        'menu_order'     => $post->menu_order
      );

      /*
       * insert the post by wp_insert_post() function
       */
      $new_post_id = wp_insert_post( $args );

      /*
       * get all current post terms ad set them to the new post draft
       */
      $taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
      foreach ($taxonomies as $taxonomy) {
        $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
        wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
      }

      /*
       * duplicate all post meta just in two SQL queries
       */
      $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
      if (count($post_meta_infos)!=0) {
        $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
        foreach ($post_meta_infos as $meta_info) {
          $meta_key = $meta_info->meta_key;
          $meta_value = addslashes($meta_info->meta_value);
          $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
        }
        $sql_query.= implode(" UNION ALL ", $sql_query_sel);
        $wpdb->query($sql_query);
      }


      /*
       * finally, redirect to the edit post screen for the new draft
       */
      wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
      exit;
    } else {
      wp_die('Post creation failed, could not find original post: ' . $post_id);
    }
  }
  add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );

  /*
   * Add the duplicate link to action list for post_row_actions
   */
  function rd_duplicate_post_link( $actions, $post ) {
    if (current_user_can('edit_posts')) {
      $actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
    }
    return $actions;
  }

  add_filter('post_row_actions', 'rd_duplicate_post_link', 10, 2);


    /*
      ****************** Custom functions ******************
    */;


    function shorten_string($string, $wordsreturned) {
      $retval = $string;
      $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
      $string = str_replace("\n", " ", $string);
      $array = explode(" ", $string);
      if (count($array)<=$wordsreturned)
      {
        $retval = $string;
      }
      else
      {
        array_splice($array, $wordsreturned);
        $retval = implode(" ", $array)." ";
      }
      return $retval;
    }
