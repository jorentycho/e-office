<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
		$next_post = get_previous_post();
		$pt = get_post_type();
		$the_query = new WP_Query(array(
		    'post_type'  => 'page',
		    'meta_key'   => '_wp_page_template',
		    'meta_value' => 'template-magazine.php'
		));
		?>


		<main>
      <script type="text/javascript">
        var post_id = <?php echo $post->ID; ?>
      </script>

			<div class="container center padding padding-m-0">
				<nav class="single_meta">
					<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<a href="<?php the_permalink(); ?>" class="button white left">magazine</a>
					<?php endwhile; wp_reset_postdata(); endif; ?>
					<?php if($next_post): ?>
						<a href="<?php echo get_permalink($next_post); ?>" class="button white right">volgend artikel</a>
					<?php endif; ?>
					<div class="clearfix"></div>
				</nav>
				<div class="content single">

					<div class="padding">
						<?php $cat = get_the_terms($post->ID, 'type'); ?>

						<span class="author">
							<?php $author_ID = get_the_author_meta('ID');
								$user_id = 'user_'. $author_ID;
								$image = get_field('image', $user_id);
								$size = 'large';
								$portrait = $image['sizes'][$size];
							?>
							<a href="<?php echo get_author_posts_url($author_ID); ?>">
								<div class="portrait" style="background-image: url('<?php echo $portrait; ?>')"></div>
							</a>
							<div class="metadata">
								<h4>
									<a href="<?php echo get_author_posts_url($author_ID); ?>">
										<?php echo get_the_author_meta( 'first_name', $author_ID); ?> <?php echo get_the_author_meta( 'last_name', $author_ID); ?>
									</a>
								</h4>
								<?php the_time('j F Y');?> <span class="views">| <span class="post_views"></span> keer bekeken </span>| <?php echo $cat[0]->name; ?>
							</div>
							<div class="clearfix"></div>

						</span>
					</div>

					<div class="col-9 col-d-12 left">

	          <div class="padding">

	            <h1 class="single"><?php the_title(); ?></h1>

							<!-- Eventdate -->
							<!-- <?php if(get_field('event_datum')):
								$event_date = get_field('event_datum'); $event_date = strtotime(str_replace("/","-",$event_date)); ?>
								<?php setlocale(LC_TIME, 'nl_NL'); ?>
								Evenement datum: <?php echo strftime('%e %B %Y', $event_date); ?>
							<?php endif; ?> -->

							<!-- Url -->
							<?php if(get_field('url')): ?>
								<a href="<?php the_field('url'); ?>">Link</a>
							<?php endif; ?>


							<!-- Video old -->
							<?php if(get_field('video_id')): ?>
								<?php if(get_field('video_platform') == 'vimeo'): ?>
									<div class="embed-container">
										<iframe src="<?php echo "//player.vimeo.com/video/" . get_field('video_id')  . "?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1" ?>" frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
									</div>
								<?php else: ?>
									<div class="embed-container">
										<iframe src="<?php echo "//www.youtube.com/embed/" . get_field('video_id')  . "?rel=0&autoplay=0" ?>" frameborder='0' allowfullscreen></iframe>
									</div>
								<?php endif; ?>
							<?php endif; ?>

							<?php the_content(); ?>

	          </div>

						<?php if (has_term( 'whitepaper', 'type' )) {

							if(get_field('acton_url')): ?>
							<div class="whitepaper__tekst">
								<?php include 'includes/module.php'; ?>
							</div>
							<div class="whitepaper__iframe padding">
								<iframe src="<?php the_field('acton_url'); ?>" frameborder="0"></iframe>
							</div>
							<?php endif;
						} else {
							include 'includes/module.php';
						}
						?>


					</div>

					<div class="col-3 col-d-12 left">
						<?php include 'includes/template-parts/social.php'; ?>
					</div>


					<div class="clearfix"></div>

					<div class="module">
						<div class="module__fp--magazine padding">
							<div id="magazine" class="center">
								<?php $orig_post = $post; global $post; $tags = wp_get_post_tags($post->ID);

									$tag_ids = array();
									foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
									$args = array(
										'tag__in' => $tag_ids,
										'post__not_in' => array($post->ID),
										'posts_per_page'=>4,
										'caller_get_posts'=>1
									);

								$the_query = new WP_Query($args);

								if ( $the_query->have_posts() ) {
										while ( $the_query->have_posts() ) {
												$the_query->the_post();


												include 'includes/template-parts/magazine_single.php';
										}
										wp_reset_postdata();
								}

							?>
							<div class="clearfix"></div>
							</div>
						</div>
					</div>


				</div>
			</div>

		</main>

	<?php endwhile; else : ?>
		<p><?php _e( 'Helaas, deze pagina is niet beschikbaar' ); ?></p>
	<?php endif; ?>


<?php get_footer(); ?>
