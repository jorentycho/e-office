document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById('watson')) {
        loadJSON();
    }

    var scroll_release = document.getElementById("scroll_release"),
        watson_conv = document.getElementById("conversation");
    if (scroll_release !== null) {
        scroll_release.onclick = function() {
            watson_conv.classList.add('scroll');
            // TODO: needs smooth scrolling
            watson_conv.scrollTop -= 20;
            scroll_release.classList.add('hide');
        }
    }

});

// Chatbot stuff
var conversationIndex = 1;
var watsonContext = "";
var count = 0;
var watson = document.getElementById('watson');

function loadJSON(printed) {

    var conversationContainer = document.getElementById("conversation");

    var questionText = document.getElementById('input').value;
    if (count == 0 && questionText.length == 0) {
        if (watson.parentElement.classList.contains('front-page')) {
            var questionText = 'front-page';
        } else if (watson.parentElement.classList.contains('fourohfour')) {
            var questionText = '404';
        }
    }
    count++;

    var dialog = new Object();
    dialog.newQuestion = questionText;

    var qTOAdd = document.createDocumentFragment();
    var qNnewDiv = document.createElement('div');
    qNnewDiv.id = 'q' + conversationIndex;
    qNnewDiv.className = 'conversationQuestion';
    var qtext = document.createElement('div');
    qtext.innerHTML = questionText;
    qNnewDiv.appendChild(qtext);
    qTOAdd.appendChild(qNnewDiv);

    if (printed != false) {
        conversationContainer.appendChild(qTOAdd);
        conversationContainer.scrollTop = conversationContainer.scrollHeight;
    }



    var data_file = "api/talktome?question=" + questionText;
    if (watsonContext != "") {
        dialog.context = watsonContext;
    }
    var http_request = new XMLHttpRequest();
    try {
        // Opera 8.0+, Firefox, Chrome, Safari
        http_request = new XMLHttpRequest();

    } catch (e) {
        // Internet Explorer Browsers
        try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");

        } catch (e) {

            try {
                http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {
                // Something went wrong
                alert("Your browser broke!");
                return false;
            }

        }
    }

    http_request.onreadystatechange = function() {
        // if(http_request.status == 0) {
        //   document.getElementById("watson").style.display = 'none';
        //   document.getElementsByClassName("module__fp--sticky-posts")[0].classList.add('visible');
        // } else {
        //   document.getElementById("watson").style.display = 'block';
        //   document.getElementsByClassName("module__fp--sticky-posts")[0].classList.remove('visible');
        // }
        if (http_request.readyState == 4) {
            // Javascript function JSON.parse to parse JSON data
            var jsonObjRaw = JSON.parse(http_request.responseText),
                jsonobjRawText = jsonObjRaw.text;


            for (i = 0; i < jsonobjRawText.length; i++) { 
                var jsonObjItem = jsonobjRawText[i];
                var jsonObj = jsonObjItem.replace(/[\[\]]+/g, '');
                var toAdd = document.createDocumentFragment();

                var newDiv = document.createElement('div');
                newDiv.id = 'r' + conversationIndex;
                conversationIndex++;
                newDiv.className = 'conversation';
                var text = document.createElement('div');
                text.innerHTML = jsonObj;
                newDiv.appendChild(text);

                watsonContext = jsonObjRaw.context;

                toAdd.appendChild(newDiv);
                //    }


                (function(index, _add) {
                    setTimeout(function() { 
                        document.getElementById("conversation").appendChild(_add);
                        conversationContainer.scrollTop = conversationContainer.scrollHeight;
                        document.getElementById('input').disabled = false;

                        document.getElementById('input').value = "";
                    }, i * 1000);
                })(i, toAdd);
            }

            // for (i = 0; i < jsonobjRawText.length; i++) { 
            //     var jsonObjItem = jsonObjRaw.text[i];
            //     var jsonObj = jsonObjItem.replace(/[\[\]]+/g, '');
            //     var toAdd = document.createDocumentFragment();
            //     //for(var i = 0; i < jsonObj.length; i++) {
            //     //      var obj = jsonObj[i];
            //     var newDiv = document.createElement('div');
            //     newDiv.id = 'r' + conversationIndex;
            //     conversationIndex++;
            //     newDiv.className = 'conversation';
            //     var text = document.createElement('div');
            //     text.innerHTML = jsonObj;
            //     newDiv.appendChild(text);
            //     //var divConversationContext = document.getElementById('conversation_context');
            //     //divConversationContext.value=jsonObj.context;
            //     watsonContext = jsonObjRaw.context;

            //     toAdd.appendChild(newDiv);
            //     //    }

            //     var time = i * 500;
            //     setTimeout(function(){ 
            //         document.getElementById("conversation").appendChild(toAdd);
            //         conversationContainer.scrollTop = conversationContainer.scrollHeight;
            //         document.getElementById('input').disabled = false;

            //         document.getElementById('input').value = "";
            //     }, time);
            // }
        }
    }
    var url = "https://eofwatsonsdkusage.mybluemix.net/api/chatbotdjl";
    http_request.open("POST", url, true);
    http_request.setRequestHeader("Content-type", "application/json");
    //http_request.open("GET", data_file, true);
    http_request.send(JSON.stringify(dialog));
}

function yesbye() {
    document.getElementById('input').value = "yes";
    loadJSON();
}

function nobye() {
    document.getElementById('input').value = "no";
    loadJSON();
}

function button(userinput, buttonContext) {
    document.getElementById('input').disabled = true;
    document.getElementById('input').value = userinput;
    watsonContext.product = buttonContext;
    loadJSON(false);
    document.getElementById('input').value = "";
}

function contact() {
  existing_elms = document.getElementById("form_0057");
  if(existing_elms != null) {
    existing_elms.parentNode.removeChild(existing_elms);
  }


  var conversationContainer = document.getElementById("conversation");

  var contact = document.createElement('div');
  contact.id = 'r' + conversationIndex;
  conversationIndex++;
  contact.className = 'conversation';

  var inner = document.createElement('div');
  inner.innerHTML = contact_form;
  contact.appendChild(inner);

  setTimeout(function(){
    conversationContainer.scrollTop = conversationContainer.scrollHeight;
  }, 100);


  document.getElementById("conversation").appendChild(contact);
}

function openlink(url) {
    var win = window.open(url);
    win.focus();
}

function process(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) { //Enter keycode
        loadJSON();
    }
}

// your custome placeholder goes here!
var ph = "Stel hier uw vraag",
    searchBar = $('#input'),
    // placeholder loop counter
    phCount = 0;

// function to return random number between
// with min/max range
function randDelay(min, max) {
    return Math.floor(Math.random() * (max-min+1)+min);
}

// function to print placeholder text in a 
// 'typing' effect
function printLetter(string, el) {
    // split string into character seperated array
    var arr = string.split(''),
        input = el,
        // store full placeholder
        origString = string,
        // get current placeholder value
        curPlace = $(input).attr("placeholder"),
        // append next letter to current placeholder
        placeholder = curPlace + arr[phCount];
        
    setTimeout(function(){
        // print placeholder text
        $(input).attr("placeholder", placeholder);
        // increase loop count
        phCount++;
        // run loop until placeholder is fully printed
        if (phCount < arr.length) {
            printLetter(origString, input);
        }
    // use random speed to simulate
    // 'human' typing
    }, randDelay(50, 90));
}  

// function to init animation
function placeholder() {
    $(searchBar).attr("placeholder", "");
    printLetter(ph, searchBar);
}

setTimeout(placeholder, 2000);
$('.submit').click(function(e){
    phCount = 0;
    e.preventDefault();
    placeholder();
});
