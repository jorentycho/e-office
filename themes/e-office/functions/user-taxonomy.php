<?php
// Register taxonomy
function register_user_taxonomy()
{
    $labels = array(
        'name' => 'Gebruiker categorie',
        'singular_name' => 'Gebruiker categorie',
        'search_items' => 'Zoek gebruiker categorieën',
        'all_items' => 'Alle gebruiker categorieën',
        'parent_item' => 'Parent gebruiker categorie',
        'parent_item_colon' => 'Parent gebruiker categorie',
        'edit_item' => 'Bewerk gebruiker categorie',
        'update_item' => 'Update gebruiker categorie',
        'add_new_item' => 'Nieuwe gebruiker categorie',
        'new_item_name' => 'Nieuwe gebruiker categorie naam',
        'menu_name' => 'Gebruiker categorie'
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'user_category')
    );

    register_taxonomy('user_category', 'user', $args);
}
add_action('init', 'register_user_taxonomy');



// Make taxonomy visible in menu
function add_user_category_menu()
{
    add_submenu_page('users.php', 'Gebruiker categorie', 'Gebruiker categorie', 'add_users', 'edit-tags.php?taxonomy=user_category');
}
add_action('admin_menu', 'add_user_category_menu');


// TODO: ACTIVE STATE OF TAXONOMY PAGE IN MENU NOT CORRECT


// Show taxonomies on profile add and edit pages
add_action('show_user_profile', 'show_user_category');
add_action('edit_user_profile', 'show_user_category');
function show_user_category($user)
{

    //get the terms that the user is assigned to
    $assigned_terms = wp_get_object_terms($user->ID, 'user_category');
    $assigned_term_ids = array();
    foreach ($assigned_terms as $term) {
        $assigned_term_ids[] = $term->term_id;
    }

    //get all the terms we have
    $user_cats = get_terms('user_category', array('hide_empty'=>false));

    echo "<h3>Gebruiker categorie</h3>";

     //list the terms as checkbox, make sure the assigned terms are checked
    foreach ($user_cats as $cat) {
        ?>
        <input type="checkbox" id="user-category-<?php echo $cat->term_id ?>" <?php if (in_array($cat->term_id, $assigned_term_ids)) {
            echo 'checked=checked';
        }
        ?> name="user_category[]"  value="<?php echo $cat->term_id;
        ?>"/>
        <?php
        echo '<label for="user-category-'.$cat->term_id.'">'.$cat->name.'</label>';
        echo '<br />';
    }
}


// Save taxonomies when saving user
add_action('personal_options_update', 'save_user_category');
add_action('edit_user_profile_update', 'save_user_category');
function save_user_category($user_id)
{

    if(!empty($_POST['user_category'])) {
      $user_terms = $_POST['user_category'];
      $terms = array_unique(array_map('intval', $user_terms));
    } else {
      $terms = null;
    }


    wp_set_object_terms($user_id, $terms, 'user_category', false);

    //make sure you clear the term cache
    clean_object_term_cache($user_id, 'user_category');

} ?>
