<?php get_header(); ?>

<main>
	<div class="container center padding padding-m-0">
		<div class="search-results">
			<div class="search-results__header">
				<div class="search-results__header--wrapper padding">
					<h2>zoekresultaten voor</h2>
					<div class="search-results__header--searchfield col-6 col-t-12">
						<form id="search" role="search" method="get" action="<?php echo get_site_url(); ?>/">
							<div id="label"><label for="search-terms" id="search-label"></label></div>
					        <div id="input"><input class="search-results__inputfield" type="text" name="s" id="search-terms" placeholder="<?php the_search_query(); ?>"></div>
						</form>
						<div id="search_trigger--results">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="14227 35 28.116 28.116" width="28px" height="28px">
							  <g transform="translate(14227 35)">
							    <path d="M27.852,26.534l-5.008-4.92a16.236,16.236,0,0,1-1.23,1.23l5.008,5.008a.849.849,0,0,0,1.23,0A.956.956,0,0,0,27.852,26.534ZM12.3,0A12.3,12.3,0,1,0,24.6,12.3,12.337,12.337,0,0,0,12.3,0Zm0,22.844A10.543,10.543,0,1,1,22.844,12.3,10.574,10.574,0,0,1,12.3,22.844Z"/>
							  </g>
							</svg>
						</div>
					</div>

				</div>
			</div>
				<ul class="search-results__ul">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<li>


						<div>
							<p class="search-results__ul--post-type">
								<?php $post_type=get_post_type();

										if($post_type ==  'post'):
											$terms = wp_get_post_terms(get_the_ID(), 'type');
											echo $terms[0]->name;
										elseif($post_type == 'auteur'):
											echo 'Wie we zijn';
										else:
											echo $post_type;
										endif;


								?>

							</p>
							<?php $post_type=get_post_type();

								if($post_type ==  'post'):
								elseif($post_type == 'auteur'):
									$image = get_field('image', 'user_'. get_the_ID());
									$size = 'large';
									$portrait = $image['sizes'][$size]; ?>

									<div class="module__user__portrait module__user__portrait--single" style="background-image: url('<?php echo $portrait; ?>')"></div>

								<?php
								else:
									
								endif;

							?>
							<a class="search-results__ul--a" href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
								<h2><?php the_title(); ?></h2>
							</a>

							<p class="search-results__ul--p">

								<?php
			            $content = get_the_content();

			            $rows = get_field('blocks');
			            $content = '';

			            if( $rows )
			            {
			              foreach( $rows as $row )
			              {
			                // debug the row

			                if($row['acf_fc_layout'] == 'single_column_text'):
			                  $content = $row['text_column'];
			                  $content = strip_tags($content, '<br />');
			                  $content = shorten_string($content, 40);

			                elseif($row['acf_fc_layout'] == 'double_column_text'):
			                  $content = $row['text_column'];
			                  $content = strip_tags($content, '<br />');
			                  $content = shorten_string($content, 40);

			                endif;
			                ?>


			                  <?php echo $content; ?>


			                <?php
			                break;
			              }
			            } else {
										$content = get_the_content();
										$content = strip_tags($content, '<br />');
										echo shorten_string($content, 40);
			            }
			          ?>
								<a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
									Lees meer
								</a>
							</p>


					</li>

					<?php endwhile; else : ?>
					<div class="no-results">
						<h2>geen resultaten gevonden</h2>
						<h3>enkele zoektips:</h3>
						<ul class="no-results__ul">
							<li><span>zorg ervoor dat alle woorden goed gespeld zijn</span></li>
							<li><span>probeer andere zoektermen</span></li>
							<li><span>maak de zoektermen algemener</span></li>
							<li><span>gebruik minder zoekwoorden</span></li>
						</ul>
					</div>
					<?php endif; ?>
				</ul>

		</div>
	</div>
</main>
<?php get_footer(); ?>
