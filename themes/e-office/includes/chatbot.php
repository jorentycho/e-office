<div id="watson">
  <script>
    contact_form = "<form id='form_0057' method='post' enctype='multipart/form-data' action='//content.e-office.com/acton/forms/userSubmit.jsp' accept-charset='UTF-8'> <input type='hidden' name='ao_a' value='18844'> <input type='hidden' name='ao_f' value='0057'> <input type='hidden' name='ao_d' value='0057:d-0001'> <input type='hidden' name='ao_p' id='ao_p' value='0'> <input type='hidden' name='ao_jstzo' id='ao_jstzo' value=''> <input type='hidden' name='ao_cuid' value=''> <input type='hidden' name='ao_srcid' value=''> <input type='hidden' name='ao_bot' id='ao_bot' value='yes'> <input type='hidden' name='ao_camp' value=''> <div id='ao_alignment_container' class='aoFormContainer' align='left'> <table class='ao_tbl_container' border='0' cellspacing='0' cellpadding='0' style='width:100%;'> <tr> <td> &nbsp;</td></tr><tr> <td class='ao_tbl_cell' style='padding-right: 10px' align='center'> <div align='left'> <div class='formField'> <div class='formFieldLabel' id='form_0057_fld_0-Label'> </div><input type='text' placeholder='voornaam' class='formFieldText formFieldLarge' id='form_0057_fld_0' name='First Name'> </div></div></td></tr><tr> <td class='ao_tbl_cell' style='padding-right: 10px' align='center'> <div align='left'> <div class='formField'> <div class='formFieldLabel' id='form_0057_fld_1-Label'> </div><input type='text' placeholder='achternaam' class='formFieldText formFieldLarge' id='form_0057_fld_1' name='Last Name'> </div></div></td></tr><tr> <td class='ao_tbl_cell' style='padding-right: 10px' align='center'> <div align='left'> <div class='formField'> <div class='formFieldLabel' id='form_0057_fld_2-Label'></div><input placeholder='mobiel nummer' type='text' class='formFieldText formFieldLarge' id='form_0057_fld_2' name='Cell Phone'> </div></div></td></tr><tr> <td> &nbsp;</td></tr><tr> <td style='padding-bottom: 10px' align='left' id='form_0057_ao_submit_button'> <a id='form_0057_ao_submit_href' class='button' href='javascript: doSubmit(document.getElementById(\"form_0057\"))'> verzend </a> </td></tr></table> </div><img src='//content.e-office.com/acton/form/18844/0057:d-0001/pgend.gif' width='1' height='1'></form>"
  </script>
  <script type="text/javascript">
      function formElementSerializers() {
          function input(element) {
              switch (element.type.toLowerCase()) {
                  case 'checkbox':
                  case 'radio':
                      return inputSelector(element);
                  default:
                      return valueSelector(element);
              }
          };

          function inputSelector(element) {
              return element.checked ? element.value : null;
          };

          function valueSelector(element) {
              return element.value;
          };

          function select(element) {
              return (element.type === 'select-one' ? selectOne : selectMany)(element);
          };

          function selectOne(element) {
              var index = element.selectedIndex;
              return index < 0 ? null : optionValue(element.options[index]);
          };

          function selectMany(element) {
              var length = element.length;
              if (!length) return null;
              var values = [];
              for (var i = 0; i < length; i++) {
                  var opt = element.options[i];
                  if (opt.selected) values.push(optionValue(opt));
              }
              return values;
          };

          function optionValue(opt) {
              if (document.documentElement.hasAttribute) return opt.hasAttribute('value') ? opt.value : opt.text;
              var element = document.getElementById(opt);
              if (element && element.getAttributeNode('value')) return opt.value;
              else return opt.text;
          };
          return {
              input: input,
              inputSelector: inputSelector,
              textarea: valueSelector,
              select: select,
              selectOne: selectOne,
              selectMany: selectMany,
              optionValue: optionValue,
              button: valueSelector
          };
      };
      var requiredFields = new Array();
      var requiredFieldGroups = new Array();
      addRequiredField = function(id) {
          requiredFields.push(id);
      };
      addRequiredFieldGroup = function(id, count) {
          requiredFieldGroups.push([id, count]);
      };
      missing = function(fieldName) {
          var f = document.getElementById(fieldName);
          var v = formElementSerializers()[f.tagName.toLowerCase()](f);
          if (v) {
              v = v.replace(/^\s*(.*)/, "$1");
              v = v.replace(/(.*?)\s*$/, "$1");
          }
          if (!v) {
              f.style.backgroundColor = '#FFFFCC';
              return 1;
          } else {
              f.style.backgroundColor = '';
              return 0;
          }
      };
      missingGroup = function(fieldName, count) {
          var result = 1;
          var color = '#FFFFCC';
          for (var i = 0; i < count; i++) {
              if (document.getElementById(fieldName + '-' + i).checked) {
                  color = '';
                  result = 0;
                  break;
              }
          }
          for (var i = 0; i < count; i++) document.getElementById(fieldName + '-' + i).parentNode.style.backgroundColor = color;
          return result;
      };
      var validatedFields = new Array();
      addFieldToValidate = function(id, validationType, arg1, arg2) {
          validatedFields.push([id, validationType, arg1, arg2]);
      };
      validateField = function(id, fieldValidationValue, arg1, arg2) {
          var field = document.getElementById(id);
          var name = field.name;
          var value = formElementSerializers()[field.tagName.toLowerCase()](field);
          for (var i = 0; i < validators.length; i++) {
              var validationDisplay = validators[i][3];
              var validationValue = validators[i][1];
              var validationFunction = validators[i][2];
              if (validationValue === fieldValidationValue) {
                  if (!validationFunction(value, arg1, arg2, id)) {
                      field.style.backgroundColor = '#FFFFCC';
                      alert(validationDisplay);
                      return false;
                  } else {
                      field.style.backgroundColor = '';
                  }
                  break;
              }
          }
          for (var i = 0; i < implicitValidators.length; i++) {
              var validationDisplay = implicitValidators[i][0];
              var validationValue = implicitValidators[i][1];
              var validationFunction = implicitValidators[i][2];
              if (validationValue === fieldValidationValue) {
                  if (!validationFunction(value, arg1, arg2, id)) {
                      field.style.backgroundColor = '#FFFFCC';
                      alert(validationDisplay);
                      return false;
                  } else {
                      field.style.backgroundColor = '';
                  }
                  break;
              }
          }
          return true;
      };
      var r = '';
      formElementById = function(form, id) {
          for (var i = 0; i < form.length; ++i)
              if (form[i].id === id) return form[i];
          return null;
      };
      doSubmit = function(form) {
          try {
              if (typeof(customSubmitProcessing) === "function") customSubmitProcessing();
          } catch (err) {}
          var ao_jstzo = formElementById(form, "ao_jstzo");
          if (ao_jstzo) ao_jstzo.value = new Date().getTimezoneOffset();
          var submitButton = document.getElementById(form.id + '_ao_submit_button');
          submitButton.style.visibility = 'hidden';
          var missingCount = 0;
          for (var i = 0; i < requiredFields.length; i++)
              if (requiredFields[i].indexOf(form.id + '_') === 0) missingCount += missing(requiredFields[i]);
          for (var i = 0; i < requiredFieldGroups.length; i++)
              if (requiredFieldGroups[i][0].indexOf(form.id + '_') === 0) missingCount += missingGroup(requiredFieldGroups[i][0], requiredFieldGroups[i][1]);
          if (missingCount >
              0) {
              alert('Please fill all required fields. ');
              submitButton.style.visibility = 'visible';
              return;
          }
          for (var i = 0; i < validatedFields.length; i++) {
              var ff = validatedFields[i];
              if (ff[0].indexOf(form.id + '_') === 0 && !validateField(ff[0], ff[1], ff[2], ff[3])) {
                  document.getElementById(ff[0]).focus();
                  submitButton.style.visibility = 'visible';
                  return;
              }
          }
          if (formElementById(form, 'ao_p').value === '1') {
              submitButton.style.visibility = 'visible';
              return;
          }
          formElementById(form, 'ao_bot').value = 'nope';
          form.submit();
      };
      if (typeof(addRequiredField) != "undefined") {
          addRequiredField("form_0057_fld_0");
      }
      if (typeof(addRequiredField) != "undefined") {
          addRequiredField("form_0057_fld_1");
      }
      if (typeof(addRequiredField) != "undefined") {
          addRequiredField("form_0057_fld_2");
      }
  </script>
  <div id="scroll_release">&uarr;</div>
  <div id="conversation"></div>
  <div>
      <input type="hidden" id="conversation_context" />
  </div>
  <div class="input">
      <input type="text" id="input" onkeypress="process(event, this)" class="left" />
      <button class="askbutton right" type="button" onclick="loadJSON()">vraag <?php // TODO: Button text ?></button>
  </div>
</div>
