<div id="modal">
  <div id="overlay">
    <div id="box" class="padding">
      <div id="close_modal">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="1313.146 5416.646 11.707 11.707">
          <g transform="translate(-10 5396.5)">
            <line id="Line_4" data-name="Line 4" class="cls-1" x1="11" y1="11" transform="translate(1323.5 20.5)"/>
            <line id="Line_5" data-name="Line 5" class="cls-1" x1="11" y1="11" transform="translate(1334.5 20.5) rotate(90)"/>
          </g>
        </svg>
      </div>
      <div id="newsletter">
        <?php the_field('newsletter_content', 'options'); ?>
      </div>
      <div id="callmeback">
        <?php the_field('cmb_content', 'options'); ?>
      </div>
      <div id="feedback">
        <?php the_field('feedback_content', 'options'); ?>
      </div>
    </div>
  </div>
</div>
