<div class="container center padding padding-m-0">
  <div class="padding">
    <h1 <?php if(get_field('light')): ?>class="light"<?php endif; ?>><?php the_title(); ?></h1>
  </div>
</div>
