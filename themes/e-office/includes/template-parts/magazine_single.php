<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'magazine_thumb' );
$ratio = 282 / 300; $height = $thumb[2] * $ratio; $height = round($height);
$types = get_the_terms($post->ID, 'type');
$themes = get_the_terms($post->ID, 'theme');?>

<div class="module left<?php foreach($types as $type) { echo ' '.$type->slug;} ?><?php if ($themes != null) { foreach($themes as $theme) { echo ' '.$theme->slug;} } ?>">
  <article class="module__magazine_single">
      <?php if(has_term('evenement', 'type') || get_field('event_datum')): ?>
        <a href="<?php echo get_permalink(); ?>" class="module__magazine_single--event">
          <div>
            <?php $event_date = get_field('event_datum'); $event_date = strtotime(str_replace("/","-",$event_date)); ?>
            <?php setlocale(LC_TIME, 'nl_NL'); ?>
            <svg class="cal" xmlns="http://www.w3.org/2000/svg" viewBox="37 16 164 34">
              <g id="Group_1" data-name="Group 1" transform="translate(-22 -3)">
                <path id="Rectangle_1" data-name="Rectangle 1" class="cls-1" d="M4,0H160a4,4,0,0,1,4,4V28a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V4A4,4,0,0,1,4,0Z" transform="translate(59 25)"/>
                <g id="Rectangle_2" data-name="Rectangle 2" class="cls-2" transform="translate(200 19)">
                  <rect class="cls-3" width="7" height="11" rx="2"/>
                  <rect class="cls-4" x="1" y="1" width="5" height="9" rx="1"/>
                </g>
                <g id="Rectangle_3" data-name="Rectangle 3" class="cls-2" transform="translate(75 19)">
                  <rect class="cls-3" width="7" height="11" rx="2"/>
                  <rect class="cls-4" x="1" y="1" width="5" height="9" rx="1"/>
                </g>
              </g>
            </svg>
            <span class="month"><?php echo strftime('%B', $event_date); ?></span>
            <span class="day"><?php echo strftime('%e', $event_date); ?></span>
          </div>
          <div class="module__magazine_single--event--overlay" style="height: <?php echo $height; ?>;"></div>
        </a>
      <?php else: ?>
        <a href="<?php echo get_permalink(); ?>" class="module__magazine_single--image">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/magazine_placeholder.jpg" class="lazy" alt="<?php the_title(); ?>" data-src="<?php echo $thumb['0'];?>" height="<?php echo $height; ?>">
          <div class="module__magazine_single--image--overlay" style="height: <?php echo $height; ?>;"></div>
        </a>
      <?php endif; ?>

      <div class="module__magazine_single--text">
        <p class="module__magazine_single--cat"><?php echo $types[0]->name; ?></p>
        <a href="<?php echo get_permalink(); ?>" class="module__magazine_single--title">
          <h3><?php the_title(); ?></h3>
        </a>
        <div class="module__magazine_single--text-p">
          <?php
            $content = get_the_content();

            $rows = get_field('blocks');

            if( $rows ) {

              if(!empty($rows[0]['text_column'])) {
                $content = $rows[0]['text_column'];
              }

            }


            $content = strip_tags($content, '<br />');
            echo '<p>'. shorten_string($content, 40) .'</p>';
          ?>
        </div>
        <a class="module__magazine_single--link" href="<?php echo get_permalink(); ?>"> lees verder</a>
      </div>
  </article>
</div>
