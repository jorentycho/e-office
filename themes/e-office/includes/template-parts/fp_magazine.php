<?php $ex_tar = new WP_Query(array(
    'post_type'  => 'page',
    'meta_key'   => '_wp_page_template',
    'meta_value' => 'template-magazine.php'
)); ?>

<div class="module">
  <div class="module__fp--magazine padding">
    <?php include 'title.php'; ?>
    <div id="magazine" class="center">
	    <?php $stickies = get_option( 'sticky_posts' );
		// Make sure we have stickies to avoid unexpected output
		if ( $stickies ) {
		    $args = [
		        'post_type'           => 'post',
		        'posts_per_page'      => 4,
		        'post__not_in'        => get_option("sticky_posts")
		    ];
		    $the_query = new WP_Query($args);

		    if ( $the_query->have_posts() ) {
		        while ( $the_query->have_posts() ) {
		            $the_query->the_post();


                include 'magazine_single.php';
		        }
		        wp_reset_postdata();
		    }
		}
	?>
  <div class="clearfix"></div>
	</div>
  <?php if ( $ex_tar->have_posts() ) : while ( $ex_tar->have_posts() ) : $ex_tar->the_post(); ?>
      <a class="module__fp--magazine--a" href="<?php the_permalink(); ?>">naar het magazine</a>
  <?php endwhile; wp_reset_postdata(); endif; ?>

  </div>
</div>
