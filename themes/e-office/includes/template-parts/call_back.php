<div class="module padding">
  <div class="module__call_back">

    <?php include 'title.php'; ?>

    <?php
      $users = get_sub_field('users');
      $i = 0;
      ?>

    <div class="module__call_back__wrap">
      <div class="module__call_back__wrap__item">
        <div class="spacer"></div>
      </div>
      <?php foreach ($users as $user) {
          $i++
        ?>
        <?php $img = get_field('image', 'user_'. $user['ID']);
          $size = 'user_s';
          if (isset($img['sizes'])):
            $img_url = $img['sizes'][$size]; else:
            $img_url = '';
          endif;
          ?>
        <div class="module__call_back__wrap__item">
          <a href="<?php echo get_author_posts_url($user['ID']); ?>">
            <img src="<?php echo $img_url; ?>" alt="<?php echo $img['alt']; ?>">
          </a>
          <div class="info">
            <h4><?php echo $user['user_firstname'];?> <?php echo $user['user_lastname'];?></h4>
            <i><?php the_field('function', 'user_'. $user['ID']);?></i>
          </div>

        </div>

      <?php
      }
      if($i = 1) { ?>
        <div class="module__call_back__wrap__item">
          <div class="empty"></div>
        </div>
      <?php }


      $text = get_sub_field('text');
      $contact = get_sub_field('contact_method');
        if ($contact == 'email') {
            ?>

          <div class="module__call_back__wrap__item">
            <a href="mailto:<?php the_sub_field('emailaddress'); ?>" class="module__call_back__wrap__item__contact circle">
              <span>
                <svg viewBox="0 0 32 26" xmlns="http://www.w3.org/2000/svg">
                    <g>
                        <path d="M29.9414,23 C29.9414,23.552 29.4944,24 28.9434,24 L2.9944,24 C2.4424,24 1.9964,23.552 1.9964,23 L1.9964,3.036 L15.2314,13.652 C15.2454,13.667 15.2384,13.689 15.2524,13.704 C15.4514,13.902 15.7094,13.989 15.9684,13.983 C16.2274,13.989 16.4864,13.902 16.6834,13.704 C16.6984,13.689 16.6914,13.667 16.7054,13.652 L29.9414,3.036 L29.9414,23 Z M28.1114,2.001 L15.9684,11.74 L3.8254,2.001 L28.1114,2.001 Z M31.9334,0.98 C31.9334,0.967 31.9254,0.957 31.9254,0.943 C31.9124,0.711 31.8224,0.503 31.6764,0.34 C31.6604,0.321 31.6684,0.293 31.6504,0.275 C31.6274,0.254 31.5934,0.262 31.5704,0.242 C31.4204,0.117 31.2374,0.039 31.0324,0.02 C30.9944,0.016 30.9634,-0.001 30.9244,2.06106088e-14 L1.0114,2.06106088e-14 C0.9724,-0.001 0.9434,0.016 0.9044,0.02 C0.7004,0.039 0.5164,0.117 0.3674,0.242 C0.3434,0.262 0.3094,0.254 0.2874,0.275 C0.2694,0.293 0.2764,0.321 0.2604,0.34 C0.1154,0.502 0.0244,0.711 0.0104,0.943 C0.0104,0.957 0.0044,0.967 0.0044,0.98 C0.0044,0.987 0.0004,0.993 0.0004,1 L0.0004,25 C0.0004,25.553 0.4464,26 0.9984,26 L30.9394,26 C31.4904,26 31.9374,25.553 31.9374,25 L31.9374,1 C31.9374,0.993 31.9334,0.987 31.9334,0.98 L31.9334,0.98 Z"></path>
                    </g>
                </svg>
              </span>
            </a>
          </div>
          <div class="module__call_back__wrap__item">
            <a href="mailto:<?php the_sub_field('emailaddress'); ?>" class="module__call_back__wrap__item__contact">
              <span>

                <?php echo $text; ?>
              </span>
            </a>
          </div>


        <?php
          } else {
        ?>
        <div class="module__call_back__wrap__item">
          <a href="#" data-modal="callmeback" class="modal_trigger module__call_back__wrap__item__contact circle">
            <span>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="3754 2510 32 32.014">
                <g transform="translate(3753.9 2510.014)">
                  <g>
                    <path d="M27.3.5a2.249,2.249,0,0,0-3.2.3c-.3.5-3.9,5.8-4.3,6.5a1.689,1.689,0,0,0-.3,1,3.429,3.429,0,0,0,.4,1.6c.3.5,1.2,2.1,1.6,2.8a40.838,40.838,0,0,1-4.2,4.7,52.546,52.546,0,0,1-4.7,4.2c-.8-.4-2.3-1.3-2.8-1.6a2.731,2.731,0,0,0-2.6-.2c-.6.4-5.9,3.9-6.4,4.3a1.865,1.865,0,0,0-.7,1.6,3.32,3.32,0,0,0,.5,1.6S4,32,5.9,32c5.5-.2,11.9-5.5,16.3-9.9s9.7-10.8,9.9-16.2h0C32,3.9,27.3.6,27.3.5ZM20.7,20.7C14.9,26.5,9.6,29.8,5.9,30a16.469,16.469,0,0,1-3.8-3.8c-.1-.2-.2-.3-.2-.5v-.1c.8-.6,5.4-3.6,6.2-4.1a.988.988,0,0,1,.6.2c.3.2,1.4.8,2.8,1.6l1.1.6,1-.7a29.779,29.779,0,0,0,5-4.4,34.63,34.63,0,0,0,4.4-5l.7-1-.6-1.1c-.6-1.1-1.3-2.4-1.6-2.8a1.191,1.191,0,0,1-.2-.6h0c.7-.8,3.8-5.5,4.4-6.3a1.087,1.087,0,0,1,.5.1,15.465,15.465,0,0,1,3.9,3.8C29.8,9.7,26.5,14.9,20.7,20.7Z"/>
                  </g>
                </g>
              </svg>
            </span>
          </a>
        </div>
        <div class="module__call_back__wrap__item">
          <a href="#" data-modal="callmeback" class="modal_trigger module__call_back__wrap__item__contact">
            <span>
              <?php echo $text; ?>
            </span>
          </a>
        </div>
        <?php } ?>
    </div>



  </div>
</div>
