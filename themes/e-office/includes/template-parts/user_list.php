<div class="module">
  <?php $terms = get_terms( 'user_category', array('hide_empty' => false));
    $cur_terms = $_GET['cat'];

    /*
    if($cur_terms != null) {
      $cur_terms = explode("_",$cur_terms);
    } else {
      $cur_terms = array();
    }
    */
  ?>
  <div class="padding">
  <div class="filter--button padding">
    filter op skills
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="-143 2710 18.985 11.009" class="filter--button-arrow">
      <g transform="translate(-142.996 2710.009)">
        <path d="M18.7.1a.494.494,0,0,0-.7.1L9.5,9.8,1,.2A.494.494,0,0,0,.3.1C0,.2-.1.5.1.7L9,10.7a.55.55,0,0,0,.5.3h0a.52.52,0,0,0,.4-.2L18.8.8C19.1.5,19,.2,18.7.1Z"></path>
      </g>
    </svg>
  </div>
    <div class="module__user_filter">
      <?php foreach ($terms as &$term) {
        $new_terms = $cur_terms;

        $new_terms = array($term->term_id);
        /*
        if(in_array($term->term_id, $new_terms)) {
          $key = array_search($term->term_id, $new_terms);
          unset($new_terms[$key]);
        } else {
          array_push($new_terms, $term->term_id);
        }
        */
        ?>
        <div>
          <a href="?cat=<?php echo implode("_",$new_terms); ?>" <?php if($cur_terms == $term->term_id) { echo "class='active'"; }; ?>><?php echo $term->name; ?> </a>
        </div>
      <?php } ?>
    </div>
  </div>
  <?php $users = get_objects_in_term( $cur_terms , 'user_category' ); ?>
  <?php $user_query = new WP_User_Query(
    array(
      'role' => '',
      'meta_key' => 'visible',
      'meta_value' => '1',
      'meta_compare' => '==',
      'include' => $users
    )
  );
    if (! empty($user_query->results)) { ?>
      <div class="module__user_list">
        <?php foreach ($user_query->results as $user) { ?>

          <?php $terms = get_the_terms($user->ID, 'user_category'); ?>

            <?php $img = get_field('image', 'user_'. $user->ID);
              $size = 'user_s';
              if(isset($img['sizes'])):
                $img_url = $img['sizes'][$size];
              else:
                $img_url = '';
              endif;
            ?>

            <div class="col-3 col-hd-4 col-d-6 col-t-4 col-m-6 left module__user_list__wrap">
              <?php if(isset($_GET['cat'])): $param = '?cat='. $_GET['cat']; else: $param = ''; endif; ?>
              <a href="<?php echo get_author_posts_url($user->ID); echo $param; ?>" class="module__user_list__wrap__user" style="background-image:url('<?php echo $img_url; ?>');">
                <div>
                  <div>
                    <h4><?php echo $user->first_name; ?> <?php echo $user->last_name; ?></h4>
                    <i><?php the_field('function', 'user_'. $user->ID); ?></i>
                  </div>
                </div>
              </a>
            </div>

          <?php //endif; ?>

        <?php } ?>
        <div class="clearfix"></div>
      </div>
    <?php } ?>

</div>
