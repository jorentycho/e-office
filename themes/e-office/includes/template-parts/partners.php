<div class="module">
  <div class="module__partners padding">
    <?php $ex_tar = null; ?>
    <?php include 'title.php'; ?>

      <div class="module__partners__wrap">

      <?php

          $args = array ('post_type' => array( 'partners' ));
          $query = new WP_Query( $args );

          if($query->have_posts()){
              while($query->have_posts()){
                  $query->the_post();
          ?>
        <div class="module__partners__wrap__logo padding">
        <a href="<?php the_permalink(); ?>">
          <?php $icon = get_field('icon');
          $icon_url = $icon['sizes']['partners']; ?>
          <img src="<?php echo $icon_url; ?>" alt="<?php echo $icon['alt']; ?>">
        </a>
        </div>

      <?php
            }
          }
      wp_reset_postdata();
      ?>

      </div>


  </div>
</div>
