<div class="module">
<article class="module__post_list_basic">
  <?php $curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
    $cat = get_the_terms($post->ID, 'type'); ?>

  <div class="module__post_list_basic__meta">
    <span class="module__post_list_basic__meta__type"><?php echo $cat[0]->name; ?></span> | <?php echo get_the_author_meta( 'first_name', $curauth->ID); ?> <?php echo get_the_author_meta( 'last_name', $curauth->ID); ?>
  </div>
  <h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
  <div>
    <?php
      $content = get_the_content();

      $rows = get_field('blocks');

      if( $rows ) {

        if(!empty($rows[0]['text_column'])) {
          $content = $rows[0]['text_column'];
        }

      }


      $content = strip_tags($content, '<br />');
      echo '<p>'. shorten_string($content, 40) .'</p>';
    ?>
  </div>
  <a href="<?php echo get_permalink(); ?>"> lees verder</a>
</article>
</div>
