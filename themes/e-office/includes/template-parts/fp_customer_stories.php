<div class="module">
  <div class="module__fp--customer_stories padding">
    <?php $ex_tar = new WP_Query(array(
        'post_type'  => 'page',
        'meta_key'   => '_wp_page_template',
        'meta_value' => 'template-klantverhalen.php'
    )); ?>
    <?php include 'title.php'; ?>

    <div class="col-6 col-d-12 left module__fp--customer_stories--left">
		<div class="stories--logos">
			<?php
		    	$taxonomy = 'my_taxonomy'; // this is the name of the taxonomy
		    	$terms = get_terms( $taxonomy, 'orderby=count&hide_empty=1' ); // for more details refer to codex please.
		    	$args = array(
		        'post_type' => 'post',
		        'posts_per_page' => 9,
		        'tax_query' => array(
		                    array(
		                        'taxonomy' => 'type',
		                        'field' => 'slug',
		                        'terms' => 'klantverhaal',
		                    )
		                )
		        );

		    	$my_query = new WP_Query( $args );
		    	if($my_query->have_posts()) : $i=0;
		         while ($my_query->have_posts()) : $my_query->the_post(); $i++; ?>

          <div class="klantverhaal__home--logo" id="logo-<?php echo $i; ?>">
            <div class="klantverhaalToggle">
              <?php $icon = get_field('klantlogo');
              $icon_url = $icon['sizes']['partners']; ?>

              <img src="<?php echo $icon_url; ?>" alt="<?php echo $icon['alt']; ?>">

            </div>
          </div>


		    <?php
		          endwhile;
		     endif;
		     wp_reset_query();
			?>


	    </div>

    <?php if ( $ex_tar->have_posts() ) : while ( $ex_tar->have_posts() ) : $ex_tar->the_post(); ?>
        <a class="module__fp--customer_stories--left--link right" href="<?php the_permalink(); ?>">alle klantverhalen</a>
    <?php endwhile; wp_reset_postdata(); endif; ?>

	</div>
    <div class="col-6 col-d-12 left module__fp--customer_stories--right">

	    	<?php
		    	$taxonomy = 'my_taxonomy'; // this is the name of the taxonomy
		    	$terms = get_terms( $taxonomy, 'orderby=count&hide_empty=1' ); // for more details refer to codex please.
		    	$args = array(
		        'post_type' => 'post',
		        'posts_per_page' => 9,
		        'tax_query' => array(
		                    array(
		                        'taxonomy' => 'type',
		                        'field' => 'slug',
		                        'terms' => 'klantverhaal',
		                    )
		                )
		        );

		    	$my_query = new WP_Query( $args );
		    	if($my_query->have_posts()) :
		         while ($my_query->have_posts()) : $my_query->the_post(); ?>

		        <div class="klantverhaal__home--tekst tekst<?php echo $j; ?>">
		      		<h3><?php the_field('klantnaam') ?> </h3>
		      		<p>
                <?php

  			        	$content = get_field('excerpt');
                  echo $content

                ?>
		      		</p>
		      		<a href="<?php the_permalink(); ?>">het verhaal van <?php the_field('klantnaam') ?></a>
		      	</div>

			<?php
		          endwhile;
		     endif;
		     wp_reset_query();
			?>


    </div>
    <div class="clearfix"></div>

  </div>
</div>
