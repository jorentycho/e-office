<div class="module" id="<?php echo $term->slug; ?>">
  <div class="module__product_cat">
    <div class="padding">
      <h3><?php echo $term->name; ?></h3>
    </div>

    <?php $the_query = new WP_Query( $args ); ?>

    <?php if ( $the_query->have_posts() ) : ?>


      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


        <div class="col-6 col-m-12 left padding">
          <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </div>




      <?php endwhile; ?>
      <div class="clearfix"></div>
      <hr />
      <?php wp_reset_postdata(); ?>



    <?php else : ?>
      <p><?php _e( 'Sorry, geen producten gevonden.' ); ?></p>
    <?php endif; ?>

  </div>
</div>
