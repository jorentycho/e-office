<?php $ex_tar = new WP_Query(array(
    'post_type'  => 'page',
    'meta_key'   => '_wp_page_template',
    'meta_value' => 'template-vision.php',
    'posts_per_page' => 1
)); ?>

<div class="module">
  <div class="module__fp--vision padding">
    <?php include 'title.php'; ?>
    <?php the_sub_field('content'); ?>
  </div>
</div>
