<div class="module">
  <div class="module__vision_block">
    <?php include 'title.php'; ?>

    <?php if( have_rows('vision_block') ):
      $i = 0;
      ?>

    	<?php while( have_rows('vision_block') ): the_row();
        $i++;

    		$image = get_sub_field('image');
        $size = 'vision';
        $image_url = $image['sizes'][$size];

    		$content = get_sub_field('content');

    		?>


        <article class="module__vision_block__item padding">
          <?php if($i % 2 == 0): ?>
            <div class="col-6 col-t-12 module__vision_block__item__image even">
              <img src="<?php echo $image_url; ?>" alt="<?php echo $image['alt']; ?>"/>
            </div>
            <div class="col-6 col-t-12 module__vision_block__item__content even">
              <?php echo $content; ?>
            </div>
          <?php else: ?>
            <div class="col-6 col-t-12 module__vision_block__item__image">
              <img src="<?php echo $image_url; ?>" alt="<?php echo $image['alt']; ?>"/>
            </div>
            <div class="col-6 col-t-12 module__vision_block__item__content">
              <?php echo $content; ?>
            </div>
          <?php endif; ?>

          <div class="clearfix"></div>
        </article>

        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/divider.png" alt="divider" class="module__vision_block__divider padding <?php if($i % 2 == 0): ?>even<?php endif; ?> <?php if($tot == $i): ?>last<?php endif; ?>">

    	<?php endwhile; ?>

    <?php endif; ?>
  </div>
</div>
