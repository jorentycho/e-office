<div class="module">
  <div class="module__content <?php the_sub_field('media'); ?>">

    <?php if (get_sub_field('media') == 'image' || get_sub_field('media') == 'video' || get_sub_field('media') == 'contact_form'): ?>


      <?php if (get_sub_field('media') == 'image'): ?>
        <div class="col-6 col-t-12 right padding">
          <!-- TODO -->
        </div>
      <?php elseif(get_sub_field('media') == 'video'): ?>
        <div class="col-6 col-t-12 right padding">
          <div class="module__content__video">
          	<?php $vid = get_sub_field('video');
            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $vid, $match)) {
              	$id = $match[1];
          	}
             ?>
             <div class="youtube" id="<?php echo $id; ?>">
            </div>
          </div>
        </div>
      <?php endif; ?>

      <div class="col-6 col-t-12 left padding">

        <?php the_sub_field('content'); ?>

      </div>

      <?php if(get_sub_field('media') == 'contact_form'): ?>
        <div class="col-6 col-t-12 left padding">
          <div class="module__content__contact_form">
            <?php include 'contact_form.php'; ?>
          </div>
        </div>
      <?php endif; ?>

      <div class="clearfix"></div>


    <?php else: ?>

      <div class="padding">
        <?php the_sub_field('content'); ?>
      </div>

    <?php endif; ?>
  </div>
</div>
