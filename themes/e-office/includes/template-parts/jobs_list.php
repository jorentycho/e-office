<div class="module">
  <div class="module__jobs_list">

    <?php if ( $the_query->have_posts() ) : ?>

      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <div class="padding module__jobs_list__item">
          <a href="<?php the_permalink(); ?>">
            <?php
              $icon = get_field('icon');
              $size = 'icon';
              $url = $icon['sizes'][$size];

            ?>
            <?php if($url) { ?>
              <img src="<?php echo $url; ?>" alt="<?php echo $url['alt']; ?>" />
            <?php } ?>

            <h3><?php the_title(); ?></h3>
            <small><?php the_field('subtitle'); ?></small>
          </a>
          <a href="<?php the_permalink(); ?>" class="module__jobs_list__item__content">
            <?php

            $rows = get_field('blocks');
            $content = '';

            if( $rows )
            {
            	foreach( $rows as $row )
            	{
            		// debug the row

                if($row['acf_fc_layout'] == 'single_column_text'):
              		$content = $row['text_column'];
                  $content = strip_tags($content, '<br />');
                  $content = shorten_string($content, 40);
                endif;
                ?>

            		<?php echo $content; ?>
                <span class="readmore">lees verder</span>

            		<?php
            		break;
            	}
            }

            ?>
          </a>
          <hr />

        </div>


      <?php endwhile; ?>
      <div class="clearfix"></div>

      <?php wp_reset_postdata(); ?>



    <?php else : ?>
      <p><?php _e( 'Sorry, geen vacatures gevonden.' ); ?></p>
    <?php endif; ?>

  </div>
</div>
