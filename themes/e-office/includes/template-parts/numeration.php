<div class="module">
  <div class="module__numeration">
    <?php if( have_rows('numeration') ): ?>

    	<?php while( have_rows('numeration') ): the_row();

    		$heading = get_sub_field('heading');
    		$content = get_sub_field('content');

    		?>

        <div class="col-4 col-t-12 left padding">
          <h3><?php echo $heading; ?></h3>
        </div>
        <div class="col-8 col-t-12 left padding">
          <?php echo $content; ?>
        </div>
        <div class="clearfix"></div>

    	<?php endwhile; ?>

    <?php endif; ?>
  </div>
</div>
