<?php $ex_tar = new WP_Query(array(
    'post_type'  => 'page',
    'meta_key'   => '_wp_page_template',
    'meta_value' => 'template-jobs.php'
)); ?>

<div class="module">
  <div class="module__fp--jobs">
    <?php $the_query = new WP_Query( array('post_type' => 'vacatures', 'posts_per_page' => 3, 'order' => 'ASC')); ?>

    <?php include 'jobs_list.php'; ?>

    <?php if ( $ex_tar->have_posts() ) : while ( $ex_tar->have_posts() ) : $ex_tar->the_post(); ?>
        <a class="module__fp--magazine--a padding" href="<?php the_permalink(); ?>">alle vacatures</a>
    <?php endwhile; wp_reset_postdata(); endif; ?>
  </div>
</div>
