<div class="module">
  <div class="module__video">
    <?php $vid = get_sub_field('video');
    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $vid, $match)) {
        $id = $match[1];
    }
    ?>
    <div class="youtube" id="<?php echo $id; ?>"></div>
  </div>
</div>
