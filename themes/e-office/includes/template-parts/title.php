<?php if(isset($ex_tar)): if ( $ex_tar->have_posts() && get_sub_field('heading')) : while ( $ex_tar->have_posts() ) : $ex_tar->the_post(); ?>
  <h2><a href="<?php the_permalink(); ?>"><?php the_sub_field('heading'); ?></a></h2>
<?php endwhile; wp_reset_postdata(); endif; elseif(get_sub_field('heading')): ?>
  <h2><?php the_sub_field('heading'); ?></h2>
<?php endif; ?>
