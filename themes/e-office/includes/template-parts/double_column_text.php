<div class="module">
  <div class="module__double_column_text">
    <div class="col-6 col-t-12 left padding">
      <?php the_sub_field('text_column'); ?>
    </div>
    <div class="col-6 col-t-12 left padding">
      <?php the_sub_field('text_column_2'); ?>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
