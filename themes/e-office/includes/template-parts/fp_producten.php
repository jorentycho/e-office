<?php $ex_tar = new WP_Query(array(
    'post_type'  => 'page',
    'meta_key'   => '_wp_page_template',
    'meta_value' => 'template-products.php'
)); ?>

<div class="module">
  <div class="module__fp--producten">
    <div class="padding">
      <?php include 'title.php'; ?>
    </div>

    <?php $terms = get_terms('productgroep', array(
        'hide_empty' => true,
        'orderby' => 'term_id',
        'number' => 6,

    )); ?>


    <div class="module__fp--producten__wrap">
      <?php foreach ($terms as $term) { ?>
        <?php $args = array(
          'post_type' => 'producten',
          'posts_per_page' => 5,
          'order' => 'ASC',
          'tax_query' => array(
            array(
              'taxonomy' => 'productgroep',
              'field'    => 'id',
              'terms'    => $term->term_id,
            ),
          ),
        ); ?>

          <div class="col-4 col-d-6 col-t-12 left padding">
            <div class="module__fp--producten__wrap__category">
              <h3>
                <a href="<?php echo get_site_url(); ?>/producten/#<?php echo $term->slug ?>"><?php echo $term->name; ?></a>
                <span>
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="-143 2710 18.985 11.009">
                    <g  transform="translate(-142.996 2710.009)">
                      <path d="M18.7.1a.494.494,0,0,0-.7.1L9.5,9.8,1,.2A.494.494,0,0,0,.3.1C0,.2-.1.5.1.7L9,10.7a.55.55,0,0,0,.5.3h0a.52.52,0,0,0,.4-.2L18.8.8C19.1.5,19,.2,18.7.1Z"/>
                    </g>
                  </svg>
                </span>
              </h3>
              <?php $the_query = new WP_Query( $args ); ?>

              <?php if ( $the_query->have_posts() ) : ?>
                <div class="wrap">

                  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>



                      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br />



                  <?php endwhile; ?>
                  <div class="clearfix"></div>
                  <?php wp_reset_postdata(); ?>

                </div>


              <?php else : ?>
                <p><?php _e( 'Sorry, geen producten gevonden.' ); ?></p>
              <?php endif; ?>

            </div>

          </div>

      <?php } ?>
    </div>
    <?php if ( $ex_tar->have_posts() ) : while ( $ex_tar->have_posts() ) : $ex_tar->the_post(); ?>
        <a class="module__fp--producten--a padding" href="<?php the_permalink(); ?>">alle producten</a>
    <?php endwhile; wp_reset_postdata(); endif; ?>
    <div class="clearfix"></div>
  </div>
</div>
