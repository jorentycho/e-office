<?php /* Template Name: People */
  get_header(); ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
		$root = 'includes/template-parts/'; ?>

		<main>
      <?php include 'includes/heading.php'; ?>

			<div class="container center padding padding-m-0">
				<div class="content">

          <div class="col-3 col-d-4 col-t-0 right padding">
            <?php $the_query = new WP_Query( array('post_type' => 'post', 'posts_per_page' => 3, 'ignore_sticky_posts' => true) ); ?>

            <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

              <?php include $root .'post_list_basic.php'; ?>

            <?php endwhile; wp_reset_postdata(); endif; ?>
          </div>
          <div class="col-9 col-d-8 col-t-12 left">
            <?php include $root .'user_list.php'; ?>
          </div>


          <div class="clearfix"></div>

				</div>
			</div>

		</main>

	<?php endwhile; else : ?>
		<p><?php _e('Helaas, deze pagina is niet beschikbaar'); ?></p>
	<?php endif; ?>


<?php get_footer(); ?>
