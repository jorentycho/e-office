<?php /* Template Name: Klantverhalen */
  get_header(); ?>

		<main>

			<?php include 'includes/heading.php'; ?>

			<div class="container center padding padding-m-0">

				<div class="content">
				<?php
			    	$args = array(
			        'post_type' => 'post',
			        'posts_per_page' => -1,
			        'tax_query' => array(
			                    array(
			                        'taxonomy' => 'type',
			                        'field' => 'slug',
			                        'terms' => 'klantverhaal',
			                    )
			                )
			        );

			    	$my_query = new WP_Query( $args );
			    	if($my_query->have_posts()) :
			         while ($my_query->have_posts()) : $my_query->the_post(); ?>
			     	<section class="klantverhaal__single padding">
			     		<div class="klantverhaal__single--underline">
				     	<div class="col-3 col-t-12 klantverhaal__single--icon left">
                <?php $icon = get_field('klantlogo');
                $icon_url = $icon['sizes']['partners']; ?>

                <a href="<?php the_permalink(); ?>"><img src="<?php echo $icon_url; ?>" alt="<?php echo $icon['alt']; ?>"></a>
				      	</div>

				        <div class="col-9 col-t-12 left klantverhaal__single-tekst">
				      		<a href="<?php the_permalink(); ?>"><h3 class="klantverhaal__single-tekst--titel"><?php the_title(); ?></h3></a>
				      		<p>
			                    <?php

								if( have_rows('blocks') ):
								    while ( have_rows('blocks') ) : the_row();
								        if( get_row_layout() == 'single_column_text' ):

								        	$content = get_sub_field('text_column');
			                        		echo wp_trim_words( $content , '40' );

								        endif;
								    endwhile;
								endif;

								?>
				      		</p>
				      	</div>
				      	</div>
			      	<div class="clearfix"></div>
			      	</section>

				<?php
			          endwhile;
			     endif;
				?>

				<div class="clearfix"></div>

				</div>

			</div>

		</main>

<?php get_footer(); ?>
