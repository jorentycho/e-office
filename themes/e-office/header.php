<!doctype html>

<html>
<head>
	<title>
	<?php if(is_front_page()):
		bloginfo('title' );
	elseif(is_author()):
		$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
		echo get_the_author_meta( 'first_name', $curauth->ID) .' '. get_the_author_meta( 'last_name', $curauth->ID) .' | e-office';
	else:
		echo get_the_title() .' | e-office';
	endif; ?>
	</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css">
	<script>
	  dataLayer = [];
	</script>
	<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-PWF928');</script>
	<!-- End Google Tag Manager -->
	<?php wp_head(); ?>
	<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png" type="image/x-icon" />
</head>
<body <?php if(is_page_template('template-thanks.php')): echo 'class="thanks"'; endif;?>>
	<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PWF928" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php if(!$_COOKIE['hide_cookie_warning']) { ?>
		<div id="cookie">
			<div class="container center padding">
				<?php the_field('cookie', 'options'); ?>
				<div id="cookie_close" class="right">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="1313.146 5416.646 11.707 11.707">
					  <g transform="translate(-10 5396.5)">
					    <line id="Line_4" data-name="Line 4" class="cls-1" x1="11" y1="11" transform="translate(1323.5 20.5)"/>
					    <line id="Line_5" data-name="Line 5" class="cls-1" x1="11" y1="11" transform="translate(1334.5 20.5) rotate(90)"/>
					  </g>
					</svg>
				</div>
			</div>
		</div>
	<?php } ?>
	<header>
		<div class="container center">
			<div class="col-2 col-d-1 col-t-4 left">
				<a href="<?php echo get_site_url(); ?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/e-office_logo.svg" alt="E-office" class="logo" width="80" height="80"/>
				</a>
			</div>
			<div class="col-9 col-d-11  col-t-1 col-m-2 left right-t">
				<div id="nav_trigger" class="right">
					<div class="bar"></div>
					<div class="bar"></div>
					<div class="bar"></div>
				</div>
				<nav id="navigation" class="header">
					<?php
						$args = array(
							'theme_location' => 'primary',
							'menu' => 'primary',
						);

						wp_nav_menu( $args ); ?>
				</nav>
			</div>
			<div class="col-1 col-d-12 col-t-7 col-m-6 right">
				<div id="search_trigger">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="14227 35 28.116 28.116" width="28px" height="28px">
					  <g transform="translate(14227 35)">
					    <path d="M27.852,26.534l-5.008-4.92a16.236,16.236,0,0,1-1.23,1.23l5.008,5.008a.849.849,0,0,0,1.23,0A.956.956,0,0,0,27.852,26.534ZM12.3,0A12.3,12.3,0,1,0,24.6,12.3,12.337,12.337,0,0,0,12.3,0Zm0,22.844A10.543,10.543,0,1,1,22.844,12.3,10.574,10.574,0,0,1,12.3,22.844Z"/>
					  </g>
					</svg>
				</div>
			</div>
		</div>
	</header>
	<div class="search_wrap">
		<div id="search_field">
			<?php get_search_form(); ?>
		</div>
	</div>

	<?php $placeholder = get_field('hero_placeholder', 'options');
	$size = 'hero';
	$placeholder = $placeholder['sizes'][$size];

	$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
	$user_id = 'user_'. $curauth->ID;

	 ?>

	 <?php if(is_search()):
		 		$hero = $placeholder;
		 	elseif(get_field('use_hero_placeholder', $user_id)):
				$hero = $placeholder;
			elseif(has_post_thumbnail()):
				$hero = get_the_post_thumbnail_url($post, 'hero');
			else:
				$hero = $placeholder;
			endif; ?>


		<div class="hero" style="background-image: url('<?php echo $hero; ?>')">
			<div class="overlay"></div>
		</div>
