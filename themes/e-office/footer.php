	<div class="container center padding padding-m-0">
		<footer>
			<div class="col-3 col-d-6 left padding">
				<a href="#" class="modal_trigger" data-modal="newsletter"><?php _e('op de hoogte blijven', 'e-office'); ?></a>
				<hr />
				<?php if(get_field('facebook', 'options')): ?>
					<div class="col-4 left">
						<a href="<?php the_field('facebook', 'options'); ?>" class="social" target="_blank"><svg viewBox="0 0 512 512"><path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"/></svg></a>
					</div>
				<?php endif; ?>
				<?php if(get_field('twitter', 'options')): ?>
				<div class="col-4 left">
					<a href="<?php the_field('twitter', 'options'); ?>" class="social" target="_blank"><svg viewBox="0 0 512 512"><path d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z"/></svg></a>
				</div>
				<?php endif; ?>
				<?php if(get_field('linkedin', 'options')): ?>
				<div class="col-4 left">
					<a href="<?php the_field('linkedin', 'options'); ?>" class="social" target="_blank"><svg viewBox="0 0 512 512"><path d="M186.4 142.4c0 19-15.3 34.5-34.2 34.5 -18.9 0-34.2-15.4-34.2-34.5 0-19 15.3-34.5 34.2-34.5C171.1 107.9 186.4 123.4 186.4 142.4zM181.4 201.3h-57.8V388.1h57.8V201.3zM273.8 201.3h-55.4V388.1h55.4c0 0 0-69.3 0-98 0-26.3 12.1-41.9 35.2-41.9 21.3 0 31.5 15 31.5 41.9 0 26.9 0 98 0 98h57.5c0 0 0-68.2 0-118.3 0-50-28.3-74.2-68-74.2 -39.6 0-56.3 30.9-56.3 30.9v-25.2H273.8z"/></svg></a>
				</div>
				<?php endif; ?>
			</div>
			<div class="col-3 col-d-6 right padding contact">
				<a href="<?php get_site_url(); ?>/contact" class="head"><?php _e('contact', 'e-office'); ?></a>
				<hr />
				<?php if(get_field('e-mail', 'options')): ?>
					<a href="mailto:<?php the_field('e-mail', 'options'); ?>"><?php the_field('e-mail', 'options'); ?></a><br />
				<?php endif; ?>
				<?php if(get_field('phone', 'options')): ?>
					<a href="tel:<?php echo preg_replace('/\s+/', '', get_field('phone', 'options')); ?>"><?php the_field('phone', 'options'); ?></a><br />
					<br />
				<?php endif; ?>


				<?php if(get_field('maps_url', 'options')): ?>
					<a href="<?php the_field('maps_url', 'options'); ?>" target="_blank">
				<?php endif; ?>
				<?php if(get_field('address', 'options')): ?>
					<?php the_field('address', 'options'); ?><br />
				<?php endif; ?>
				<?php if(get_field('zip_city', 'options')): ?>
					<?php the_field('zip_city', 'options'); ?><br />
					<br />
				<?php endif; ?>
				<?php if(get_field('maps_url', 'options')): ?>
					</a>
				<?php endif; ?>


				<?php if(get_field('kvk', 'options')): ?>
					KvK <?php the_field('kvk', 'options'); ?><br />
				<?php endif; ?>
				<?php if(get_field('btw', 'options')): ?>
					BTW <?php the_field('btw', 'options'); ?>
				<?php endif; ?>
			</div>
			<div class="col-3 col-d-0 left padding">
				<?php $the_query = new WP_Query( array('post_type' => 'producten', 'posts_per_page' => 8)); ?>

		    <?php if ( $the_query->have_posts() ) : ?>
					<?php $products_main = new WP_Query(array(
					    'post_type'  => 'page',
					    'meta_key'   => '_wp_page_template',
					    'meta_value' => 'template-products.php'
					)); ?>

					<?php if ( $products_main->have_posts() ) : while ( $products_main->have_posts() ) : $products_main->the_post(); ?>
							<a href="<?php the_permalink(); ?>"><?php _e('producten', 'e-office'); ?></a>
					<?php endwhile; wp_reset_postdata(); endif; ?>
					<hr />
					<div class="products">
			      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

			      <?php endwhile; ?>
			      <div class="clearfix"></div>
					</div>

		      <?php wp_reset_postdata(); ?>

		    <?php else : ?>
		      <p><?php _e( 'Sorry, geen producten gevonden.' ); ?></p>
		    <?php endif; ?>
			</div>
			<div class="col-3 col-d-0 left padding">
				<?php $the_query = new WP_Query( array('post_type' => 'vacatures', 'posts_per_page' => 4)); ?>

		    <?php if ( $the_query->have_posts() ) : ?>
					<?php $jobs_main = new WP_Query(array(
					    'post_type'  => 'page',
					    'meta_key'   => '_wp_page_template',
					    'meta_value' => 'template-jobs.php'
					)); ?>

					<?php if ( $jobs_main->have_posts() ) : while ( $jobs_main->have_posts() ) : $jobs_main->the_post(); ?>
							<a href="<?php the_permalink(); ?>"><?php _e('vacatures', 'e-office'); ?></a>
					<?php endwhile; wp_reset_postdata(); endif; ?>
					<hr />
					<div class="jobs">
			      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

			      <?php endwhile; ?>
			      <div class="clearfix"></div>
					</div>

		      <?php wp_reset_postdata(); ?>

		    <?php else : ?>
		      <p><?php _e( 'Sorry, geen vacatures gevonden.' ); ?></p>
		    <?php endif; ?>
				<nav class="footer">
					<?php
						$args = array(
							'theme_location' => 'footer',
							'menu' => 'footer',
						);

						wp_nav_menu( $args ); ?>
				</nav>
			</div>
			<div class="clearfix"></div>
		</footer>
		<div class="copyright">
			<span>© Copyright <?php echo date("Y"); ?> e-office bv</span>

			<?php
			$args = array(
				'theme_location' => 'mini footer',
				'menu' => 'mini footer',
			);

			wp_nav_menu( $args ); ?>
		</div>
	</div>

	<?php include 'includes/feedback.php'; ?>
	<?php include 'includes/modal.php'; ?>

	<link rel="dns-prefetch stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700">
	<script defer src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>
	<?php if(is_front_page() || is_404()): ?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/watson.js"></script>
	<?php endif; ?>
	<?php if(is_page_template('template-magazine.php')): ?>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/magazine.js"></script>
	<?php endif; ?>
	<?php if(is_page_template('template-contact.php')): ?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRWbnYh3Mg_L-ePBXc3Z-rm_L5ZkCxOlw"></script>
		<script>
				var map;

				initMap();
				function initMap() {
					map = new_map(document.getElementById('map'));
				}

				function new_map(el) {

						var marker = document.getElementById('marker');

						var eoff_sty =[
						    {
						        featureType: "poi",
						        elementType: "labels",
						        stylers: [
						              { visibility: "off" }
						        ]
						    }
						];

						var args = {
								zoom: 16,
								center: new google.maps.LatLng(0, 0),
								mapTypeId: google.maps.MapTypeId.ROADMAP,
								styles: eoff_sty,
								mapTypeControl: false,
								zoomControl: false,
								scaleControl: false,
								streetViewControl: false,
								draggable: false,
						    scrollwheel: false,
						    panControl: false,
						};

						map = new google.maps.Map(el, args);

						map.markers = [];

						add_marker(marker, map);


						return map;
				}

				function add_marker(marker, map) {

						var marker = new google.maps.Marker({
								position: {lat: parseFloat(marker.dataset.lat), lng: parseFloat(marker.dataset.lng)},
								map: map,
								icon: '<?php echo get_stylesheet_directory_uri(); ?>/images/marker.png'
						});

						var bounds = new google.maps.LatLngBounds();

						var latlng = new google.maps.LatLng(marker.position.lat()-0.001, marker.position.lng());

						bounds.extend(latlng);


						map.setCenter(bounds.getCenter());
						map.setZoom(16);


						google.maps.event.addListener(marker, 'click', function () {
							window.open(
							  '<?php the_field('maps_url', 'options'); ?>',
							  '_blank' // <- This is what makes it open in a new window.
							);
						});

				}



		</script>
	<?php endif; ?>
	<?php wp_footer(); ?>

</body>
</html>
