<?php /* Template Name: Magazine */
  get_header(); ?>

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<main>
      <?php include 'includes/heading.php'; ?>

			<div class="container center padding padding-m-0">
				<div class="content magazine">
          <div class="padding">
            <?php the_content(); ?>
          </div>

          <?php $args = array (
            'post_type' => 'post',
            'posts_per_page' => -1,
          );
          $the_query = new WP_Query( $args ); ?>

          <?php if ( $the_query->have_posts() ) : ?>

            <div class="padding">
              <div class="filters">
                <div class="col-8 col-d-12 left">
                  <span class="text">filter op</span>
                  <?php $types = get_terms( array(
                      'taxonomy' => 'type',
                      'hide_empty' => false,
                  ) ); ?>
                  <span class="select_wrap">
                    <select class="button-group filters-button-group" data-filter-group="type">
                      <option value="">alle types</option>
                      <?php foreach ($types as $type) { ?>
                        <option value=".<?php echo $type->slug; ?>"><?php echo strtolower($type->name); ?></option>
                      <?php } ?>
                    </select>
                  </span>
                  <span class="text">over</span>
                  <?php $themes = get_terms( array(
                      'taxonomy' => 'theme',
                      'hide_empty' => false,
                  ) ); ?>
                  <span class="select_wrap">
                    <select class="button-group filters-button-group" data-filter-group="theme">
                      <option value="">alle thema's</option>
                      <?php foreach ($themes as $theme) { ?>
                        <option value=".<?php echo $theme->slug; ?>"><?php echo strtolower($theme->name); ?></option>
                      <?php } ?>
                    </select>
                  </span>
                </div>
                <div class="col-4 col-d-0 left">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="14227 35 28.116 28.116" width="28px" height="28px" class="right">
        					  <g transform="translate(14227 35)">
        					    <path d="M27.852,26.534l-5.008-4.92a16.236,16.236,0,0,1-1.23,1.23l5.008,5.008a.849.849,0,0,0,1.23,0A.956.956,0,0,0,27.852,26.534ZM12.3,0A12.3,12.3,0,1,0,24.6,12.3,12.337,12.337,0,0,0,12.3,0Zm0,22.844A10.543,10.543,0,1,1,22.844,12.3,10.574,10.574,0,0,1,12.3,22.844Z"/>
        					  </g>
        					</svg>
                  <input type="text" class="quicksearch right" placeholder="zoeken" />

                </div>
                <div class="clearfix"></div>
              </div>





              <div id="magazine" class="center">


                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                  <?php include 'includes/template-parts/magazine_single.php'; ?>

                <?php endwhile; ?>

              </div>

            </div>


          <?php wp_reset_postdata(); else : ?>
          	<p><?php _e( 'Geen berichten gevonden' ); ?></p>
          <?php endif; ?>

				</div>
			</div>
		</main>

	<?php endwhile; else : ?>
		<p><?php _e('Helaas, deze pagina is niet beschikbaar'); ?></p>
	<?php endif; ?>


<?php get_footer(); ?>
