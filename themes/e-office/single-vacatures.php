<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
		$next_post = get_previous_post();
		$pt = get_post_type();
		$the_query = new WP_Query(array(
		    'post_type'  => 'page',
		    'meta_key'   => '_wp_page_template',
		    'meta_value' => 'template-jobs.php'
		));
		?>


		<main>

			<div class="container center padding padding-m-0">
				<nav class="single_meta">
					<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<a href="<?php the_permalink(); ?>" class="button white left">alle vacatures</a>
					<?php endwhile; wp_reset_postdata(); endif; ?>
					<?php if($next_post): ?>
						<a href="<?php echo get_permalink($next_post); ?>" class="button white right">volgende vacature</a>
					<?php endif; ?>
					<div class="clearfix"></div>
				</nav>
				<div class="content single">

          <div class="padding">
            <h1 class="single"><?php the_title(); ?></h1>
            <?php $cat = get_the_terms($post->ID, 'productgroep'); ?>

            <span class="category">
              <?php echo $cat[0]->name; ?>
            </span>
            <hr />
          </div>

            <?php include 'includes/module.php'; ?>

						<?php if(get_field('apply_now')): ?>
							<div class="padding">
								<a class="button module__apply_now" href="<?php the_field('apply_now'); ?>">Solliciteer nu!</a>
							</div>
						<?php endif; ?>
				</div>
			</div>

		</main>

	<?php endwhile; else : ?>
		<p><?php _e( 'Helaas, deze pagina is niet beschikbaar' ); ?></p>
	<?php endif; ?>


<?php get_footer(); ?>
